This is a set of programs solving the Smoluchowski-type kinetic equations using the fast algorithms of linear and polylinear algebra.

Codes are developed for compilation with Intel Compilers (icc, icpc) using MKL subroutines.

This program set is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.

======================================================================================================================================================
Contributors:

Sergey Matveev       -- project leadership, libtt, all of solvers
Dmitry Zheltkov      -- libtt, solvers
Alexander Smirnov    -- mass flow Monte Carlo
Nadezda Ampilogova   -- Anderson acceleration

======================================================================================================================================================
Directory structure:

./                           -- root of all the subroutines
./libtt/                     -- basic classes for work with the low-rank matrices and low-rank tensors in TT-format
                               
./solvers/                   -- directory with the solvers of various mathematical models of aggregation and fragmentation processes
                               
./solvers/one-dimensional    -- solvers of one-component Smoluchowski-type kinetic eqautions of the aggregation and fragmentation processes:
                                
                                cross_1d.cpp              -- fast solver of the continuous Smoluchowski coagulation equation based on use of the 
                                                             low-rank matrices; algorithm is published at JCP [1].
                                                          
                                direct_1d.cpp             -- regular slow solver of the continuous Smoluchowski coagulation equation.
                                                             
                                cross_colm.cpp            -- fast solver of Colm Connaughhton's model; the model was published at PRL [4].
                                                            
                                cross_colm_continuous.cpp -- fast solver of the continuous version of Colm Connaughton's model.
                                                           
                                cross_brill.cpp           -- fast solver of Nikolai Brilliantov's model [2]; the model was published at PNAS [5].
                                parallel_brill.cpp        -- fast MPI-using solver of Nikolai Brilliantov's model [6-7].
                                stationary_brill.cpp      -- fast solver searching steady-state solutions of the model by Nikolai Brilliantov using
                                                             the fixed-point iterations method.
                                                             
                                soil_dynamic.cpp          -- fast dynamic solver of the local soil aggregation model.
                                                             
                                soil_stationary.cpp       -- fast steady-state solver by fixed-point iterations of the local soil aggregation model.
                                
                                anderson.cpp              -- Anderson acceleration of fixed point iterations based solver of Nikolai Brilliantov's model
                                                             will be enlarged soon as an important tool finding steady-state distributions of aggregation
                                                             and fragmentation models (new, in progress, tests etc).
                                                          
./solvers/mass_flow/         -- mass flow Monte Carlo solver of the multicomponent Smoluchowski coagulation equation. 
                                published at Atomic Energy [3]. uses libpgplot.a
                                                          
./solvers/multidimensional/  -- finite-difference solvers of multicomponent Smoluchowski coagulation equation based models.
                                
                                tt_Smoluh_2.cpp           -- slow TT-based solver of the continuous Smoluchowski equation
                                direct_2d.cpp             -- direct predictor-corrector scheme solving the continuous strictly 2-component Smolushowski equation
                                Smoluh.py                 -- fast TT-based solver of the continuous multicomponent Smoluchowski equation [9-10].
                                                             Requires the ttpy package [8] installation.
                                QTT_Smoluh.py             -- fast QTT solver of the continuous multicomponent Smoluchowski equation (not ready yet)
                                                             Requires the ttpy package [8] installation.
======================================================================================================================================================

References:

1. Matveev S.A., Smirnov A.P. and Tyrtyshnikov E.E. 
   "A fast numerical method for the Cauchy problem for the Smoluchowski equation."
   Journal of Computational Physics (2015)
   
2. Matveev S.A., Smirnov A.P., Tyrtyshnikov E.E., Brilliantov N.V.
   "A fast numerical method for solving the Smoluchowski-type kinetic equations of aggregation and fragmentation processes."
   Vychislitel'nye Metody i Programmirovanie  (2014)
   
3. A. A. Sorokin, V. F. Strizhov, M. N. Demin, A. P. Smirnov
   "Monte-Carlo Modeling of Aerosol Kinetics."
   Atomic Energy (2015)
   
4. Ball R.C., et al.
   "Collective oscillations in irreversible coagulation driven by monomer inputs and large-cluster outputs."
   Physical review letters (2012)
   
5. Brilliantov N. V., et al. 
   "Size distribution of particles in Saturn’s rings from aggregation and fragmentation."
   Proceedings of the National Academy of Sciences (2015)
   
6. Matveev S.A.
   "A Parallel Implementation of a Fast Method for Solving the Smoluchowski-Type Kinetic Equations of Aggregation and Fragmentation Processes."
   Vychislitel'nye Metody i Programmirovanie (2015)
   
7. Zheltkov D.A. Tyrtyshnikov E.E
   "A parallel implementation of the matrix cross approximation method", 
   Vychislitel'nye Metody i Programmirovanie (2015)
   
8. Oseledets I.V., Saluev T.G., Savostyanov D.V., Dolgov S.V.
   Tensor Train Toolbox, python version
   https://github.com/oseledets/ttpy
   
9. Smirnov A.P., Matveev S.A., Zheltkov D.A., and Tyrtyshnikov E.E.
   "Fast and accurate finite-difference method solving multicomponent smoluchowski coagulation equation with source and sink terms."
   Procedia computer science, 80:2141–2146 (2016) 
   
10. Matveev S.A., Zheltkov D.A., Tyrtyshnikov E.E., and Smirnov A.P.
    "Tensor train versus monte carlo for the multicomponent smoluchowski coagulation equation"
    Journal of Computational Physics, 316:164–179, (2016)
    
======================================================================================================================================================
