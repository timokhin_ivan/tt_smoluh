import numpy as np
import tt
import rect_cross
import time
import scipy.special

d = 2
n = 20
N = 2**n
D = n * d
r = 1 
h = 100.0/(N - 1)
tau = 1e-2
N_steps = 10 
tolerance = 1e-6
print_res = 0
T = N_steps * tau
check_error = 1

def Coag_K(x):
    
    if (x.size % 2) != 0:
        print "Kernel must depend on even number of indeces!"
        exit()

    #u = (x[:, :d ] + 1e-2) * h
    #v = (x[:, d: ] + 1e-2) * h

    u = x[:, 0:D:n ] * 0
    v = x[:, D: 2*D:n] * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
        v = 2 * v + x[:, (D + i):(2*D):n]
    u = u * h + 1e-2
    v =v * h + 1e-2
    #return np.exp( -v.sum(axis = 1) - u.sum(axis = 1))
    #return np.exp(-x.sum(axis = 1) * h)
    # Ballistic kernel
    # (u^(1/3) + v^(1/3))^2 * sqrt(1/u + 1/v)
    #return (np.power(u.sum(axis = 1), 1.0 / 3.0) + np.power(v.sum(axis = 1), 1.0 / 3.0))**2.0 *  np.power( 1.0 / u.sum(axis = 1) + 1.0 / v.sum(axis = 1), 0.5)
    #Generalized multiplication a = 0.1
    return np.power(u.sum(axis = 1), 0.1) * np.power(v.sum(axis = 1), -0.1) + np.power(u.sum(axis = 1), -0.1) * np.power(v.sum(axis = 1), 0.1)
    #return np.power(x.sum(axis = 1), 0.1)
#============================================================================================
#============================================================================================
def Check_n(x):
    u = x[:, 0:D:n ] * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
    return np.exp(-u.sum(axis = 1))

def Check_Mass(x):
    u = x[:, 0:D:n ] * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
    return u.sum(axis = 1)

def Start_Cond(x):
    u = x[:, 0:D:n ] * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
    return np.exp(-u.sum(axis = 1))

def Analytical(x):
    u = (x[:, 0:D:n ]) * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
    return np.exp(-u.sum(axis=1)) / (1.0 + T / 2.0)**2 * scipy.special.i0(  2.0 * np.sqrt( np.prod(u, axis = 1)  * T / (2.0 + T)))

def Analyt_Convol(x):
    u = (x[:, 0:D:n ]) * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
#    return x.prod(axis=1) * h**d
    return np.exp(-u.sum(axis = 1)) * np.prod(u, axis = 1)
    """
    a = np.exp(-x.sum(axis = 1) * h)
    for i in xrange(d):
        a = a * (np.ones(x.shape[0]) - np.exp(-x[:, i] * h))
    return a
    """    
def Analyt_Fredgolm(x):
    u = (x[:, 0:D:n ]) * 0
    for i in xrange(n):
        u = 2 * u + x[:, i:D:n]
    u = u * h
    return np.exp(-u.sum(axis = 1)) * (1.0 - np.exp( -(N - 1) * h))**d
#============================================================================================
#============================================================================================
#============================================================================================
#============================================================================================
def Second_Integral(TT_solution, Coag_Kernel):
    # create weights of rectangular quadrature like tt.ones * h**d;
    # we multiply each carriage by h to avoid the machine zero
    weights = tt.ones(2, D)
    carriages = tt.tensor.to_list(weights)
    for i in xrange(d):
        carriages[i] = carriages[i] * h
#        carriages[i][:,0,:] /= 2
#        carriages[i][:,N-1,:] /= 2
    weights = tt.tensor.from_list(carriages)
    #create list of carriages of coagulation kernel
     
    carriages = tt.tensor.to_list(Coag_Kernel)
    
    #list of first d carriages
    list_v = carriages[ : D]
    #list if last d - 1 carriages
    list_u = carriages[ D + 1 :]
    Integral = np.zeros(Coag_Kernel.r[D])
    #print 'Coagulation kernel in TT format'
    #print Coag_Kernel 
    #print '============================================================'
    for alpha in xrange(carriages[D - 1 ].shape[2]):
        #print 'alpha', alpha
        #insert one slice from d+1-th carriage to create TT
        list_u.insert(0, carriages[D] [alpha : alpha + 1, :, :])
        #get tensor trains from the lists
        #K_v_part = tt.tensor.from_list(list_v)
        K_u_part = tt.tensor.from_list(list_u)
        #compute Integrand (u) = K_u(u) * n(u) 
        K_u_part = tt.multifuncrs([K_u_part, TT_solution], lambda x: np.prod(x, axis=1), eps = tolerance)
        #compute I[alpha] = \int_0^{v_{max}} Integrand[alpha](u) du will be a vector of size r_d
        # sum of results over the middle d-th index
        Integral[alpha] = tt.dot(K_u_part, weights)
        list_u.pop(0)
    #============================================================================================
    list_v[D - 1] = list_v[D - 1].reshape(list_v[D - 1].shape[0] * list_v[D - 1].shape[1], list_v[D - 1].shape[2])
    list_v[D - 1] = list_v[D - 1].dot(Integral)
    list_v[D - 1] = list_v[D - 1].reshape(carriages[D - 1].shape[0], carriages[D - 1].shape[1], 1)
    I2 = tt.tensor.from_list(list_v) 
    I2 = tt.multifuncrs([I2, TT_solution], lambda x: np.prod(x, axis = 1), eps = tolerance)
    
    #print '============================================================'
    #print 'Coagulation Kernel'
    #print Coag_Kernel
    return I2
#============================================================================================
#============================================================================================
#============================================================================================
#============================================================================================
def First_Integral(TT_solution, Coag_Kernel): 
    carriages = tt.tensor.to_list(Coag_Kernel)
    
    #list of last d - 1 carriages
    list_u = carriages[D + 1  :]
    #list of first d - 1 carriages
    list_v = carriages[ : D - 1]
    
    for alpha in xrange(Coag_Kernel.r[D]):#Loop along the middle index of kernel
        #print 'alpha', alpha
        #insert one slice from d+1-th carriage and create TT
        list_u.insert(0, carriages[D] [alpha : alpha + 1, :, :])
        #append one slice from d-th carriage and create TT
        list_v.append(carriages[D - 1][:, :, alpha : alpha + 1])
        #get tensor trains from the lists
        K_u_part = tt.tensor.from_list(list_u)
        K_v_part = tt.tensor.from_list(list_v)
        
        #compute Integrands  
        
        K_u_part = tt.multifuncrs([K_u_part, TT_solution], lambda x: x.prod(axis = 1), eps = tolerance) # K_alpha (u) * n(u)
        K_v_part = tt.multifuncrs([K_v_part, TT_solution], lambda x: x.prod(axis = 1), eps = tolerance) # K_alpha(v) * n(v)
        K_u_part = tt.tensor.to_list(K_u_part)
        K_v_part = tt.tensor.to_list(K_v_part)
        
        for i in xrange(D):
            # add the required zeros, multiply by the integration mesh step
#            K_u_part[i][:, 0, :] = K_u_part[i][:, 0, :] / 2.0 
#            K_v_part[i][:, 0, :] = K_v_part[i][:, 0, :] / 2.0
            K_u_part[i] = np.concatenate( (K_u_part[i], np.zeros([K_u_part[i].shape[0], 1, K_u_part[i].shape[2]])), axis = 1)
            # do the first FFT
            K_u_part[i] = np.fft.fft(K_u_part[i], axis = 1, n = 3)
            # add the required zeros and multiply by the integration mesh step
            K_v_part[i] = np.concatenate( (K_v_part[i], np.zeros([K_v_part[i].shape[0], 1, K_v_part[i].shape[2]])), axis = 1)
            # do the first FFT
            K_v_part[i] = np.fft.fft(K_v_part[i], axis = 1, n = 3)
            
        # Build tensor trains after the axis-wide FFTs
        K_u_part = tt.tensor.from_list(K_u_part)
        K_v_part = tt.tensor.from_list(K_v_part)
        
         
        # Elementwise product of 2 TTs; Store the results in K_u_part(allows us not to make new TT but replase the old one)
        K_u_part = tt.multifuncrs([K_u_part, K_v_part], lambda x: x.prod(axis = 1), eps = tolerance)

        # Back to list of TT-cores
        K_u_part = tt.tensor.to_list(K_u_part)
        
        for i in xrange(D):
            # Inverse FFT along each phase-spase axis
            K_u_part[i] = np.fft.ifft(K_u_part[i], axis = 1, n = 3)
            # Truncation. Here we delete the additional (n-1)-zeros
            K_u_part[i] = h *  K_u_part[i][:, : 2, :]
        # Restore the TT
        K_u_part = tt.tensor.from_list(K_u_part)
        # Save the results in I1
        if alpha == 0:
            I1 = K_u_part
        else:
            I1 = tt.multifuncrs([I1, K_u_part], lambda x: x.sum(axis = 1), eps = tolerance)
        #delete used slices before addition of the new ones
        list_v.pop(D - 1)
        list_u.pop(0)
        #print 'I1'
        #print I1
    #============================================================================================
    I1 = tt.multifuncrs([I1], lambda x: x.real, eps = tolerance)
    return I1
#============================================================================================
#============================================================================================
#============================================================================================
#============================================================================================
"""

x0 = tt.rand(N, d, r)
Analyt_conv = rect_cross.cross(Analyt_Convol, x0, nswp = 6, kickrank = 1, rf = 2)


Check_conv = tt.ones(N, d)

x0 = tt.rand(N, 2 * d, r)
Check_conv_kernel = rect_cross.cross(Coag_K, x0, nswp = 6, kickrank = 1, rf = 2)
#Check_conv_kernel = tt.ones(N, 2 * d)

Check_conv = First_Integral(Check_conv, Check_conv_kernel)
Error = Check_conv - Analyt_conv

print 'Convolution relative error = ', Error.norm() / Analyt_conv.norm()

x0 = tt.rand(N, 2 * d, r)
Check_conv_kernel = rect_cross.cross(Coag_K, x0, nswp = 6, kickrank = 1, rf = 2)

x0 = tt.rand(N, d, r)
#Check_fredgolm = rect_cross.cross(Check_n, x0, nswp = 6, kickrank = 1, rf = 2)
Check_fredgolm = tt.ones(N, d)
Check_fredgolm = Second_Integral(Check_fredgolm, Check_conv_kernel)

x0 = tt.rand(N, d, r)
Analyt_fredgolm = rect_cross.cross(Analyt_Fredgolm, x0, nswp = 6, kickrank = 1, rf = 2)

#Analyt_fredgolm = tt.ones(N, d) * (1.0 - np.exp(-(N - 1) * h))**d # ((N - 1) * h)**d
Error = Check_fredgolm - Analyt_fredgolm
print 'Fredgolm error', Error.norm() / Analyt_fredgolm.norm()

quit()
exit()
"""
#approximate starting condition
print 'Approximate starting condition'
x0 = tt.rand(2, D, r)
TT_Solution = rect_cross.cross(Start_Cond, x0, nswp = 50, kickrank = 1, rf = 2)
TT_Solution = TT_Solution.round(tolerance)
print 'Approximate TT for check of total mass'
Check_mass  = rect_cross.cross(Check_Mass, x0, nswp = 50, kickrank = 1, rf = 2)
Check_mass = Check_mass.round(tolerance)
#TT_Analyt   = rect_cross.cross(Analytical, x0, nswp = 6, kickrank = 1, rf = 2)
#print TT_Analyt

weights = tt.ones(2, D)
carriages = tt.tensor.to_list(weights)
for i in xrange(d):
    carriages[i] = carriages[i] * h
#    carriages[i][:, 0, :] /= 2
#    carriages[i][:, N - 1, :] /= 2
weights = tt.tensor.from_list(carriages)


Mass_tt = tt.multifuncrs([TT_Solution, Check_mass], lambda x: np.prod(x, axis = 1 ), eps = tolerance)
print 'Starting mass = ', tt.dot(Mass_tt, weights)
print 'Approximate kernel'
#test with ballistic kernel
#
x0 = tt.rand(2, 2 * D, r)
#Coag_Kernel = rect_cross.cross(Coag_K, x0, nswp = 6, kickrank = 1, rf = 2)
#Coag_Kernel = Coag_Kernel.round(tolerance)
#print Coag_Kernel.erank
#print '============================================================'
#
#test with K(u,v) = 1
#
Coag_Kernel = tt.ones(2, 2 * D)

t1 = time.clock()
print 'alpha = ', Coag_Kernel.r[d] 

for t in xrange(N_steps):
    print '=============================================================='
    print 'Step', t + 1
    print '=============================================================='
    
    First_integral = 0.25 * tau * First_Integral(TT_Solution, Coag_Kernel)
    
    Second_integral = - tau * 0.5 * Second_Integral(TT_Solution, Coag_Kernel)
    print 'First integral predictor'
    print First_integral.erank
    print 'Second integral predictor'
    print Second_integral.erank
    
    TT_Solution_predictor = tt.multifuncrs([TT_Solution, First_integral, Second_integral], lambda x: np.sum(x, axis = 1) , eps = tolerance)
    
    First_integral = 0.5 * tau * First_Integral(TT_Solution_predictor, Coag_Kernel)
    
    Second_integral = - tau * Second_Integral(TT_Solution_predictor, Coag_Kernel)
    
    print 'First integral corrector'
    print First_integral.erank
    print 'Second integral corrector'
    print Second_integral.erank
    
    TT_Solution = tt.multifuncrs([TT_Solution, First_integral, Second_integral], lambda x:  np.sum(x, axis = 1), eps = tolerance)
        
    Mass_tt = tt.multifuncrs([TT_Solution, Check_mass], lambda x: np.prod(x, axis = 1), eps = tolerance)
    print 'mass = ', tt.dot(Mass_tt, weights)
    print 'Solution'
    print TT_Solution.erank
    print '=============================================================='
    
#    if print_res and (N <= 20000):
#        print 'saving results into file %s.dat'%(t)
#        res = open("%s.dat"%(t), 'w')
#        for x in xrange(N):
#            res.writelines("\n")
#            for y in xrange(N):
#                #print x*h, ' ', y*h, ' ', (x + y) * h * TT_Solution[x,y]
#                res.writelines("%s %s %s\n"%(y * h, x * h, (x + y) * h * TT_Solution[y,x].real))
#        res.close()
    
    if check_error and (((t + 1) % 10) == 0):
        T = (t + 1) * tau
        x0 = tt.rand(2, D, r)
        TT_Analyt   = -rect_cross.cross(Analytical, x0, nswp = 6, kickrank = 1, rf = 2)
        Error = tt.multifuncrs([TT_Solution, TT_Analyt], lambda x: np.sum(x, axis = 1), eps = tolerance)
        print 'TT_Analyt ranks at T = ', T
        print TT_Analyt.erank
        print 'Relative error', Error.norm() / TT_Analyt.norm()
        print 'V_max =', N * h, 'tau =', tau, 'N_steps = ', N_steps
        #Mass_tt = tt.multifuncrs([TT_Analyt, Check_mass], lambda x: np.prod(x, axis = 1 ), eps = tolerance)
        #print 'Analyt mass = ', tt.dot(Mass_tt, weights)


#if (N <= 2000) and print_res and (t == N_steps - 1):
#    res = open("Analyt.dat", 'w')
#    for x in xrange(N):
#        res.writelines("\n")
#        for y in xrange(N):
#            #print x*h, ' ', y*h, ' ', (x + y) * h * TT_Solution[x,y]
#            res.writelines("%s %s %s\n"%(y * h, x * h, -(x + y) * h * TT_Analyt[y,x]))
#    res.close()


t2 = time.clock()
print 'time = ', t2 - t1
