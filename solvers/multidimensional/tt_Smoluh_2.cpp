#include "tt_Smoluh_2.h"
#include "../../libtt/tensor_train.h"

#include <iostream>
#include <cstdio>
#include <string.h>
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <mkl.h>
#include <omp.h>
#include <ctime>
#include <gsl/gsl_sf_bessel.h>
using namespace std;

//***********************************************************************************
//***********************************************************************************
//			Global & static  parameters; Kernel of integral equation; Analytic sollution

double* StartCondition::weights=NULL;
double* StartCondition::knots = NULL;

double* FirstIntegral::weights=NULL;
double* FirstIntegral::knots = NULL;
int* FirstIntegral::average_ranks = NULL;
int FirstIntegral::number_of_computations = 0;

double* SecondIntegral::weights= NULL;
double* SecondIntegral::knots= NULL;
int* SecondIntegral::average_ranks = NULL;
int SecondIntegral::number_of_computations = 0;

double* ForVolumeCheck::knots=NULL;

double* Steps::weights=NULL;
double* Steps::knots = NULL;
double Steps::mesh_step=1e-1;
double Steps::step_time=1e-2;

long int Steps::number_of_computations=0;

double* TDiscreteAnalitycal::knots = NULL;

double K(double v, double u)
{
/*	if ((u == 0) ) return K(1e-1, v);
	if ((v == 0) ) return K(u, 1e-1);
	else return M_PI*pow(0.75/M_PI, 2.0l/3)*pow((pow(v, 1.0l/3) + pow(u, 1.0l/3)), 2)*sqrt(8.0l/M_PI * (1.0l/v + 1.0l/u));
*/
	return 1.0;
}


double Analytic (double v1, double v2, double t)
{
	
	return exp (-v1-v2) /((1.0+t/2)*(1.0+t/2)) * gsl_sf_bessel_I0(2.0* sqrt (t* v1* v2/(2.0+t)));
	
}
//***********************************************************************************
//***********************************************************************************
//			Constructors
StartCondition::StartCondition(const int &d, int* &m): TTensor(d, m)
{
}

StartCondition::StartCondition(): TTensor()
{
}

FirstIntegral::FirstIntegral(const int &d, int* &m): TTensor(d, m)
{
}

FirstIntegral::FirstIntegral(): TTensor()
{
}
SecondIntegral::SecondIntegral():TTensor()
{
}

SecondIntegral::SecondIntegral(const int &d, int* &m ):TTensor(d,m)
{
}

Steps::Steps():TTensor()
{
}

Steps::Steps(const int &d, int* &m ):TTensor(d,m)
{
}


ForVolumeCheck::ForVolumeCheck():TTensor()
{
}

ForVolumeCheck::ForVolumeCheck(const int &d, int* &m):TTensor(d,m)
{
}

Weights::Weights(): TTensorTrain()
{
}

Weights::Weights(const TTensorTrain &tens, const double &c):TTensorTrain(tens, c)
{
}

Weights::Weights(const TTensorTrain &tens, const double *c):TTensorTrain(tens, c)
{
}

TDiscreteAnalitycal::TDiscreteAnalitycal():TTensor()
{
}

TDiscreteAnalitycal::TDiscreteAnalitycal(const int &d, int* &m ):TTensor(d,m)
{
}
//***********************************************************************************
//***********************************************************************************
//			Methods
void FirstIntegral::change_point(int *point) 
{
//changes poing for the fisrt integral operator
//within threre is a correction of modes sizes for this array
	for (int i=0; i< this->get_dimensionality(); i++ )
	{
		this->modes_sizes[i]=point[i] + 1;
	}

}

void SecondIntegral::change_point(int *p)
{
	for (int i=0; i<this->get_dimensionality(); i++)
	{
		this->point[i]=p[i];
	}

}



void Weights::change_point(int *point)
{
	for (int i=0; i< this->get_dimensionality(); i++)
	{
		this->modes_sizes[i]=point[i] + 1;
	}
}

void FirstIntegral::set_to_integrate (TTensorTrain *train)
{
	this->to_integrate=train;
}

void Steps::set_trains(TTensorTrain *prev_train, TTensorTrain *half_train, char is_half)
{
	this->is_half_step = is_half;
	this->previous = prev_train;
	this->half_step = half_train;
}

void SecondIntegral::set_to_integrate (TTensorTrain *train)
{
	this->to_integrate=train;
}

void Steps::set_parameters(double tolerance, int iterations_number)
{
	parameters.tolerance = tolerance;
	parameters.maximal_iterations_number = iterations_number;
}

//***********************************************************************************
//***********************************************************************************
// 		Operators [  ]  <--- main part for method

double StartCondition::operator[](int *i)
{
	double u = 0;


	for (int j = 0; j < this->get_dimensionality(); j++)
	{
		u += knots[i[j]];
	}
	return exp(-u);

}

double FirstIntegral::operator[](int *i)
{
// gives value of integrand for the fisrt integral operator in a knot of mesh.
// this->to_integrate is stored in TT-fortmat
// first integral is computed in [0;v_1]x[0;v_2]x...x[0;v_d] limits
	double u = 0;
	double v = 0;
	int _point[this->get_dimensionality()];

	for (int j = 0; j < this->get_dimensionality(); j++)
	{
		u += knots[i[j]];
		v += knots[this->get_mode_size(j) -1] ;
		_point[j]= this->get_mode_size(j)-1-i[j];
	}
	return ((*(this->to_integrate))[i]) * ((*(this->to_integrate))[_point])* K(v - u, u);

}


double SecondIntegral::operator[](int* i)
{
// gives value of integrand for the second integral operator in a knot of mesh.
// this->to_integrate is stored in TT-fortmat
// second integral is computed in [0; v_max] x ... [0; v_max] limits
	double u = 0;
	double v = 0;

	for (int j=0; j < this->get_dimensionality();j++)
	{
		u += knots [i[j]];
		v += knots [ this->point[j] ] ;
	}
	return ((*(this->to_integrate))[i]) * K(v, u);
}

double ForVolumeCheck::operator[](int *i)
{
	double v=0;
	for (int j=0; j<this->get_dimensionality();j++)
	{
		v += knots[i[j]];
	}
	return v;
}

double Steps::operator[](int* i)
{
//computes elements for results of predictor/corrector steps

//if you have got such kernel: K(0,u)=0 check your point before computation of the second integral operator in this point(integrand is zero functiotn).

	
	bool first_zero = false;
	bool second_zero = false;
	double vol=1;
	double full_vol=1;//pow(max_knot,double(this->get_dimensionality()));
	double v=0;
	int *m_s, *m_s_sec;
	int d = this->get_dimensionality();

	number_of_computations++;

	m_s = (int*)malloc (this->get_dimensionality()*sizeof(int));
	m_s_sec = (int*)malloc (this->get_dimensionality()*sizeof(int));

	for (int j=0; j<this->get_dimensionality(); j++)
		if (i[j]==0)
		{	
			first_zero=true;	
		}
	for (int j=0; j<this->get_dimensionality(); j++)
	{
		m_s[j] = this->get_mode_size(j);
		m_s_sec[j]= this->get_mode_size(j);
		vol *= knots[i[j]];
		v += knots[i[j]];
	}

	double integral1;
	//			Integral 1
	FirstIntegral::number_of_computations++;
	
	if(!first_zero)
	{	
		FirstIntegral first_integral(d, m_s );
		first_integral.set_to_integrate(this->half_step);
		first_integral.point=(int *) malloc ((this->get_dimensionality())*sizeof(int));
		first_integral.change_point(i);

		TTensorTrain first_tt;

		first_tt.Approximate(&first_integral, this->parameters);
		TTrapecialWeights first_weights(dimensionality, i, mesh_step);
		
		for (int local=0; local<dimensionality;local++ )
			FirstIntegral::average_ranks[local]+=first_tt.get_rank(local);

		integral1 = first_tt.dot(first_weights);
	}
	else integral1=0;

	// if K(0,u)=0 then integrand for second integral operatod is 0.
	//			Integral 2
	if (v==0)
	{
		second_zero=true;
	}
	double integral2;
	SecondIntegral::number_of_computations++;
	//if(!second_zero)
	{

		SecondIntegral second_integral(d, m_s_sec);
		second_integral.set_to_integrate(this->half_step);
		
		second_integral.point=(int*) malloc((this->get_dimensionality())*sizeof(int));
		second_integral.change_point(i);
		
		TTensorTrain second_tt;
		
		second_tt.Approximate(&second_integral, this->parameters);

		int tmp[dimensionality];
		for (int k = 0; k < dimensionality; k++) tmp[k] = modes_sizes[k] - 1;
		TTrapecialWeights second_weights(dimensionality, tmp, mesh_step);

		for (int local=0; local < dimensionality; local++)
			SecondIntegral::average_ranks[local]+=second_tt.get_rank(local);

		integral2 = second_tt.dot(second_weights)*full_vol;
	}
	//else integral2=0;

	//			Compute a result
	if (this->is_half_step)
		return step_time*(0.5*integral1*full_vol - ( (*(this->previous))[i] ) * integral2)*0.5 + ( (*(this->previous))[i] );
	else
		return step_time*(0.5*integral1*full_vol - ( (*(this->half_step))[i] ) * integral2)+ ( (*(this->previous))[i] );
}





double TDiscreteAnalitycal::operator[](int *i)
{
	double v1 = knots[i[0]];
	double v2 = knots[i[1]];
	
	return Analytic(v1,v2,this->t);

}
