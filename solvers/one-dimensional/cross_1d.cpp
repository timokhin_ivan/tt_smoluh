#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"
#include <sys/time.h>
using namespace std;

double K(const int & u, const int &v, const double h)
{
	return 1.0;

	double u1=u * h;
	double v1=v * h;
//	return 2 * (pow (u1*v1, 1.0l/3.0) + 1.0);

	if (u == 0)
		return K(1, v, h/2.0);
	if (v == 0)
		return K(u, 1, h/2.0);

//	double u1=u * h;
//	double v1=v * h;
//	return pow( ( pow(u1, -1.0l/3) + pow(v1, -1.0l/3) ) *  ( pow(u1, 1.0l/3) + pow(v1, 1.0l/3) )  , 3.0l);
//	return 2 * (pow (u1*v1, 1.0l/12.0));
//	return pow(u1, 0.95l)*pow(v1, -0.95l) + pow (u1, -0.95l)* pow (v1, 0.95l);
        return /*M_PI * pow(0.75 / M_PI, 2.0l/3) */ pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt( /*8.0l / M_PI */ (1.0l / u1 + 1.0l / v1));
//	return u1 * v1;
//    return 1.0;
}

class TKernel: public TMatrix {

        public:
                double h;
                TKernel (const int &, const int &);
                double value (const int & , const int &);

};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
        
}

double TKernel::value(const int &i, const int &j)
{
        return h * K(i, j, h);
}


double L1(const int &N, const int &i, const double *n, const double &h)
{
    double l1 = 0;
    for (int i1 = 1; i1 <= i; i1++)
    {
        l1 += n[i1] * n[i - i1] * K((i - i1) , i1 ,h) + n[i1 - 1] * n[i - i1 + 1] * K((i - i1 + 1) , (i1 - 1) ,h);
    }
    l1 *= h/2;
    return l1;
}




double L2(const int &N, const int &i, const double *n, const double &h)
{
    double l2 = 0;
    for (int i1 = 1; i1 < N; i1++)
    {
        l2 += n[i1] * K(i , i1,h) + n[i1 - 1] * K(i , (i1 - 1), h );
    }
    l2 *= h/2;
    return l2;
}


double L_for_vol(const int &N, const double *n, const double &h)
{
    double l2 = 0;
    for (int i1 = 1; i1 < N; i1++)
    {
        l2 += n[i1] * ( i1 * h ) + n[i1 - 1] * ( (i1-1) * h );
    }
    l2 *= h/2;
    return l2;
}

double start_cond(const double &v, const double &h)
{
	return exp (-v) ;
}

double analitycal_solution(const double &v, const double & t, const double & h)
{
//	for the constant kernel
	return (1.0 / ((1.0 + t / 2.0) * (1.0 + t / 2.0))) * exp(-v / (1.0 + t / 2.0));
}

int main(int argc, char** argv)
{
        if (argc != 8)
        {
                printf("7 parameters have been expected\n");
                printf("v_max n_steps time_step n_knots tol num_threads ifpaint\n");
                return -1;
        }
	int num_of_threads = 1;
	char if_with_slow = 0;
	double h;
        double v_max = 10.0;
        int N = 101, steps = 100;
        double time_step = 0.01;
        char name_of_file[15] = "values.dat";
	char for_monte_carlo_file[100];
        char name_of_picture[30];
        double tolerance = 1e-6;
        FILE* values, *mass_dynamics, *analit_values;
	int ifpaint = 0;
	struct timeval full_start;
        TCross_Parallel_v1_Parameters parameters;
	
        sscanf(argv[1], "%lf", &v_max);
        sscanf(argv[2], "%d", &steps);
        sscanf(argv[3], "%lf", &time_step);
        sscanf(argv[4], "%d", &N);
        sscanf(argv[5], "%lf", &tolerance);
	sscanf(argv[6], "%d", &num_of_threads);
	sscanf(argv[7], "%d", &ifpaint);

	mass_dynamics = fopen ("mass_dynamics.dat", "w+");
	h = v_max / (N - 1);
        if (ifpaint)
	        system("cd IMG; rm *.png; cd ..");
//	for fast algorithm
        double *n_k, *n_k_half, *n_k_one, *n_analit, *S;
        double *L1_res_vec, *L2_res_vec, *L1_correction, *L2_correction;

        n_k = (double *) malloc (N * sizeof(double));
        n_k_half = (double *) malloc (N * sizeof(double));
        n_k_one = (double *) malloc (N * sizeof(double));
        n_analit = (double *) malloc (N * sizeof(double));
        S = (double *) malloc (N * sizeof(double));

	L1_correction = (double *)malloc (N * sizeof(double));
	L2_correction = (double *)malloc (N * sizeof(double));

	omp_set_num_threads(num_of_threads);
	gettimeofday(&full_start, NULL);
       	TKernel kernel(N, N);
        kernel.h=h;
        parameters.tolerance=tolerance;
        parameters.maximal_iterations_number = 0;
        
        TCross_Parallel_v1 crossed_kernel;
        crossed_kernel.Approximate(&kernel, parameters);
	struct timeval start, end;
	double r_time = 0.0;
//	for slow algorithm
	double *n_k_direct, *n_k_half_direct, *n_k_one_direct, *n_analit_direct;
	double *difference = (double *) malloc (N * sizeof(double));

	struct timeval start_direct, end_direct;
	double r_time_direct = 0.0;
	if (if_with_slow)
	{
		n_k_direct = (double *) malloc(N * sizeof(double));
		n_k_half_direct = (double *) malloc (N * sizeof(double));
		n_k_one_direct = (double *) malloc (N * sizeof(double));
		n_analit_direct = (double *) malloc (N * sizeof(double));

	}	
//	starting conditions
        for (int i = 0; i < N; i++)
        {
                n_k[i] = start_cond(h * i, h);
		if (if_with_slow)
			n_k_direct[i] = start_cond(h *i, h);
        }
	gettimeofday(&start, NULL);
//	start scheme
        for (int k = 0; k < steps; k++)
        {
//                printf("step %d \n", k);
// ================================================================================================
// ================================================================================================
// 		fast computations of L1 and L2 
                L2_res_vec = crossed_kernel.matvec(n_k);
                L1_res_vec = crossed_kernel.smol_conv_trapezoids(n_k);
//		compute corrections for L2 into trapecial weights
                for (int i = 0; i < N; i++)
                {
                        L2_correction[i] = ( K( i, 1 ,h ) * n_k[0] + K(i, N ,h) * n_k[N - 1] ) * h;
			n_analit[i] = analitycal_solution(h*i, (k + 0.5) * time_step, h);
			if (if_with_slow)
				n_analit_direct[i] = analitycal_solution(h*i, (k + 0.5) * time_step, h);
                }
                cblas_daxpy(N, -0.5, L2_correction, 1, L2_res_vec, 1);
//		predictor step

                for (int i = 0; i < N; i++)
                {
//			slow
			if (if_with_slow)
				n_k_half_direct[i] = ( L1(N, i, n_k_direct, h) * 0.5 - n_k_direct[i] * L2(N, i, n_k_direct, h) ) * 0.5 * time_step + n_k_direct[i];
//			fast
                        n_k_half[i] = ( L1_res_vec[i] * 0.5 - n_k[i] * L2_res_vec[i] ) * 0.5 * time_step + n_k[i];
			if (n_k_half[i] < 0.0)	
				n_k_half[i] = 0.0;
                }
//		check error
		double norm = cblas_dnrm2(N, n_analit, 1);
		cblas_daxpy(N, -1.0, n_k_half, 1, n_analit, 1);

		//printf ("Relative error fast = %E \n", cblas_dnrm2(N, n_analit, 1) / norm);

		if (if_with_slow)
		{
			cblas_daxpy(N, -1.0, n_k_half_direct, 1, n_analit_direct, 1);
			printf ("Relative error slow = %E \n", cblas_dnrm2(N, n_analit_direct, 1) / norm);
		}
                free (L2_res_vec);
                L2_res_vec = NULL;
                free (L1_res_vec);
                L1_res_vec = NULL;
// ================================================================================================
// ================================================================================================
		if (ifpaint && ( (steps < 1000) || (k % 100 == 0) ))
		{
	                values=fopen (name_of_file, "w+");
			analit_values = fopen ("analytical.dat", "w+");
		}
//		fast computations of L1 and L2
                L2_res_vec=crossed_kernel.matvec(n_k_half);
                L1_res_vec=crossed_kernel.smol_conv_trapezoids(n_k_half);
//		compute corrections for L2 into trapecial weights
                for (int i = 0; i < N; i++)
                {
                        L2_correction[i] = ( K( i, 1, h ) * n_k_half[0]  + K(i, N ,h) * n_k_half[N-1]) * h;
			n_analit[i] = analitycal_solution(h*i, (k + 1.0) * time_step, h);

			if (if_with_slow)
				n_analit_direct[i] = analitycal_solution(h * i, (k + 1.0) * time_step, h);

			if (ifpaint && ( (steps < 1000) || (k % 100 == 0) && (i % 10 == 0)))
				fprintf(analit_values, "%E %E \n", h * (i + 1), h * (i + 1) * n_analit[i] );
	        }
		if (ifpaint && ( (steps < 1000) || (k % 100 == 0) ))
			fclose(analit_values);
                cblas_daxpy(N, -0.5, L2_correction, 1, L2_res_vec, 1);
//		corrector step
                for (int i = 0; i < N; i++)
                {
//			slow
			if (if_with_slow)
				n_k_one_direct[i] = (L1(N, i, n_k_half_direct, h) * 0.5 - n_k_half_direct[i] * L2(N, i, n_k_half_direct, h)) * time_step + n_k_direct[i];
//			fast
		    	S[i] = (L1_res_vec[i] * 0.5 - n_k_half[i] *  L2_res_vec[i]) ;
                        n_k_one[i] = S[i] * time_step + n_k[i];
			if (n_k_one[i] < 0.0)
				n_k_one[i] = 0.0;
			if (ifpaint && ( (steps < 1000) || (k % 100 == 0) ) && (i % 10 == 0))
                        	fprintf(values, "%E %E \n", h * (i + 1), h * (i + 1) * n_k_one[i]);
                }
                free (L2_res_vec);
                L2_res_vec = NULL;
                free (L1_res_vec);
                L1_res_vec = NULL;
//		paint pictures
		if (ifpaint && ((steps < 1000) || (k % 100 == 0)) )
		{
				fclose(values);
				system("gnuplot plot_cross_1d");
				sprintf(name_of_picture, "mv 1.png IMG/file%03d.png",k + 1);
				system(name_of_picture);
		}
//		check volume conservation, error, difference between fast and slow
//		printf("monomers concentration = %lf\n", n_k_one[0]);
                double volume = L_for_vol(N, n_k_one, h);
                double analit_volume = L_for_vol(N, n_analit, h);
		fprintf (mass_dynamics, "%lf \n", volume);

		norm = cblas_dnrm2(N, n_analit, 1);
		cblas_daxpy(N, -1.0, n_k_one, 1, n_analit, 1);
		//printf ("Relative error fast = %E \n", cblas_dnrm2(N, n_analit, 1) / norm);

		if (if_with_slow)
		{
			cblas_daxpy(N, -1.0, n_k_one_direct, 1, n_analit_direct, 1);
			printf ("Relative error slow = %E \n", cblas_dnrm2(N, n_analit_direct, 1) / norm);


			for (int i = 0; i < N; i ++ )
				difference[i] = n_k_one_direct[i] ;

			cblas_daxpy(N, -1.0, n_k_one, 1, difference, 1);

			double diff_norm = cblas_dnrm2(N, difference, 1);
			printf("absolute difference norm2 = %E \n", diff_norm);
			printf("relative difference norm = %E \n", diff_norm / cblas_dnrm2(N, n_k_one, 1));

			double *tmp_direct = n_k_one_direct;
			n_k_one_direct = n_k_direct;
			n_k_direct = tmp_direct;
			tmp_direct = NULL;
		}
		double * tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;

        }
	gettimeofday(&end, NULL);
	r_time = end.tv_sec - start.tv_sec + ((double) (end.tv_usec - start.tv_usec)) / 1000000;
	FILE *log = fopen("IMG/info.log", "w+");
	fprintf (log, "====================================\n");
	fprintf (log, "Average time of step = %lf seconds\n", r_time/ (steps+1) );
	fprintf (log, "Scheme time = %lf seconds\n", r_time );
	fprintf (log, "====================================\n");
	r_time = end.tv_sec - full_start.tv_sec + ((double) (end.tv_usec - full_start.tv_usec)) / 1000000;
	fprintf (log, "Full time = %E seconds\n", r_time );

	r_time = start.tv_sec - full_start.tv_sec + ((double) (start.tv_usec - full_start.tv_usec)) / 1000000;

	fprintf (log, "====================================\n");
	fprintf (log, "Approx time = %E seconds \n", r_time);
        fprintf(log, "Rank = %d; \n Tolerance = %e \n", crossed_kernel.get_rank(), tolerance);
	fprintf (log, "====================================\n");
	fprintf(log, "Input parameters\n n_steps = %d; \n time_step = %lf; \n v_max = %lf; \n n_knots = %d; \n", steps, time_step, v_max, N);
	fclose(log);
	system("mv mass_dynamics.dat IMG/ \n");
        if (ifpaint)
                system("mv values.dat IMG/ \n");

        free(n_k);
        free(n_k_half);
        free(n_k_one);
        free(n_k_direct);
        free(n_k_half_direct);
        free(n_k_one_direct);
        free(n_analit);

	fclose(mass_dynamics);
}
