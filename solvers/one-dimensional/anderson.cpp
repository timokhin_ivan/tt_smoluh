#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <sys/time.h>
#include <mkl.h>
#include "../../libtt/tensor_train.h"
#include "./anderson_lib.h"
using namespace std;

// can not change
int n = 131072;

// maybe can change
double _a;

// can change
double lambda;

////////////////////////////////////////////////////
double K(const int &u, const int &v) {
    double u1 = u + 1.0;
    double v1 = v + 1.0;
    return pow(u1, _a) * pow(v1, -_a) + pow(u1, -_a) * pow(v1, _a);
}
class TKernel: public TMatrix {
public:
    TKernel(const int &, const int &);
    double value(const int &, const int &);
    int kernel_type;
};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
}

double TKernel::value(const int &i, const int &j)
{
    if (this->kernel_type == 1)
        return K(i, j);
    if (this->kernel_type == 2)
        return K(i + 1, j + 1);
    else
        return K(i, j);
}

TCross_Parallel_v1_Parameters parameters;
TKernel kernel(n, n);
TCross_Parallel_v1 crossed_kernel;
TKernel monomers_kernel(n - 1, n - 1);
TCross_Parallel_v1 crossed_mono_kernel;
////////////////////////////////////////////////////
// Smoluchowski operator 
////////////////////////////////////////////////////
void calculate_gg(double *n_k, double  *n_k_one, int N)
{
    double Q = 1.0;
    double *L1_res_vec, *L2_res_vec;
    double *workspace_for_mono, *L3_for_mono_vec, *workspace_for_source;
    double L3_for_mono, L4_for_mono;

    workspace_for_mono = (double *) malloc ((N - 1) * sizeof(double));
    // fast computations of L1, L2, L3 and L4
    L1_res_vec = crossed_kernel.smol_conv_discrete(n_k);
    L2_res_vec = crossed_kernel.matvec(n_k);
    L3_for_mono = 0.0;
    L4_for_mono = 0.0;
    for (int i = 1; i < N; i++)
    {
        workspace_for_mono[i - 1] = (i + 1) * n_k[i];
        L4_for_mono += K(0, i) * n_k[i] * (i + 1);
    }

    L3_for_mono_vec = crossed_mono_kernel.matvec(workspace_for_mono);
    for (int i = 1; i < N; i++)
        L3_for_mono += n_k[i] * L3_for_mono_vec[i - 1];

    L4_for_mono *= lambda * n_k[0];

    workspace_for_source = crossed_kernel.matvec(n_k, 'p');
    double J = 0.0;
    for (int i = 0; i < N; i++)
        J += workspace_for_source[i] * n_k[i] * (i + 1);
    J = 0.0;
    for (int i = 0; i < N; i++)
    {
        // fast
        if (i > 0)
            n_k_one[i] = (L1_res_vec[i] * 0.5 -
                          (1.0 + lambda) * n_k[i] * L2_res_vec[i]) + n_k[i];
        else
            n_k_one[i] = (-n_k[i] * L2_res_vec[i] +
                          L3_for_mono * lambda + L4_for_mono + J + Q) + n_k[i];
        if (n_k_one[i] < 0.0)
            n_k_one[i] = 0.0;
    }    
    
    free(L1_res_vec);
    L1_res_vec = NULL;
    free(L2_res_vec);
    L2_res_vec = NULL;
    free(L3_for_mono_vec);
    L3_for_mono_vec = NULL;
    free(workspace_for_source);
    workspace_for_source = NULL;
    free(workspace_for_mono);
    workspace_for_mono = NULL;
}


double start_cond(const int &v)
{
    return 0.0;
    if (v < 100)
        return 1.0 / 5050.0;
    else
        return 0.0;
}

int main(int argc, char** argv)
{
    // maximum number of iterations
    int max_iter = 10;
    // anderson parameter
    int mmax = 1;
    // current number of previous layers
    int mcur = -1;
    // tolerance of the residual norm
    double eps = 1e-8;
    // problem's dimension
    //int n = 131;
    double tolerance = 1e-6;
    double cond_param = 1e-6;
    char datadir[100]="test_f", command_string[500];

    if (argc != 8) {
        printf("7 parameters have been expected\n");
        // 100 1e-12 0 0.4 5e-2 1e-2 test_f
        printf("n_iter eps m_anderson a_param");
        printf(" lambda cond_param datadir\n");
        return -1;
    }

    sscanf(argv[1], "%d", &max_iter);
    sscanf(argv[2], "%lf", &eps);
    sscanf(argv[3], "%d", &mmax);
    sscanf(argv[4], "%lf", &_a);
    sscanf(argv[5], "%lf", &lambda);
    sscanf(argv[6], "%lf", &cond_param);
    sscanf(argv[7], "%s", datadir);

    sprintf(command_string, "mkdir %s", datadir);
    system(command_string);

    parameters.tolerance = tolerance;
    parameters.maximal_iterations_number = 0;

    kernel.kernel_type = 1;
    crossed_kernel.Approximate(&kernel, parameters);

    monomers_kernel.kernel_type = 2;
    crossed_mono_kernel.Approximate(&monomers_kernel, parameters);
    double r_time = 0.0;
    double res_norm = -1.0;

    double * init = (double *) malloc(n * sizeof(double));
    for (int i = 0; i < n; i++)
	init[i] = start_cond(i);
    double *final_result = anderson_acceleration(n, max_iter, mmax, eps, cond_param, init, calculate_gg, &res_norm, &r_time);

    char final_data_file[200];
    sprintf(final_data_file, "%s/final.dat", datadir);
    FILE *final_data = fopen(final_data_file, "w+");

    for (int i = 0; i < n; i++)
	fprintf(final_data, "%d %E\n", i+1, final_result[i]);
    fclose(final_data);

    char log_file[200];
    sprintf(log_file, "%s/info.log", datadir);
    FILE * log = fopen(log_file, "w+");
    fprintf(log, "N_equations %d\n", n);
    fprintf(log, "m = %d\n", mmax);
    fprintf(log, "a = %f\n", _a);
    fprintf(log, "lambda = %lf\n", lambda);
    fprintf(log, "cond = %E\n", cond_param);
    //fprintf(log, "N_iterations = %d\n", iter - 1);
    fprintf(log, "eps = %E\n", res_norm);
    fprintf(log, "========================================\n");
    fprintf(log, "========================================\n");
    fprintf(log, "runtime = %lf\n", r_time);
    fprintf(log, "rank = %d\n", crossed_kernel.get_rank());
    fclose(log);
    //free_vec(u, mmax + 2);
    //free_vec(g, mmax + 1);
    free(final_result);
    return 0;
}



