#include "../../libtt/parallel_cross.h"
#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include <sys/time.h>
using namespace std;

double K(const int & u, const int &v)
{
//	return 1.0;
	double u1=u+1.0;
	double v1=v+1.0;
//	return pow(u1*v1, 1.0l/8.0l);
//	return pow(u1, 0.75l) * pow(v1, -0.75l) + pow (u1, -0.75l) * pow (v1, 0.75l);
	return M_PI * pow(0.75 / M_PI, 2.0l/3) * pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt(8.0l /M_PI * (1.0l / u1 + 1.0l / v1));
}

class TKernel: public TMatrix {
        public:
		int kernel_type ;
                TKernel (const int &, const int &);
                double value (const int & , const int &);

};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
}

double TKernel::value(const int &i, const int &j)
{
	return K(i, j);
}

double start_cond(const int &v)
{
//	return 0.0;
	if ( (v < 10)  )
		return 1.0 / 10;
	else
		return 0.0;
}

void get_full_mass(double *&n, int &N)
{
	int p, np;
	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	double loc_mass = 0, full_mass;

	for ( int l = 0; l < N / np; l++)
		loc_mass += ( (l + 1 + p * N / np) * n[l] );
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Allreduce(&loc_mass, &full_mass, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	if ( p == 0)
		printf("total mass = %lf \n", full_mass);
	
	MPI_Barrier(MPI_COMM_WORLD);	
}

void get_full_density(double *&n, int &N)
{
	int p, np;
	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	double loc_density = 0, full_density;

	for ( int l = 0; l < N / np; l++)
		loc_density +=  n[l];

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Allreduce(&loc_density, &full_density, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	if ( p == 0)
		printf("total density = %lf \n", full_density);
	MPI_Barrier(MPI_COMM_WORLD);	
}	

int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);

	int full_N = 1024, steps = 100, ifpaint = 0;
	double time_step = 1e-2, lambda = 1e-2, tolerance = 1e-6;
	char datadir[100] = "IMG";
	if (argc != 8)
	{
		printf("7 parameters have been expected\n");
		printf("n_steps time_step n_equations tolerance lambda datadir ifpaint\n");
		exit(0);
	}
	
        sscanf(argv[1], "%d", &steps);
        sscanf(argv[2], "%lf", &time_step);
        sscanf(argv[3], "%d", &full_N);
        sscanf(argv[4], "%lf", &tolerance);
	sscanf(argv[5], "%lf", &lambda);
	sscanf(argv[6], "%s", datadir);
	sscanf(argv[7], "%d", &ifpaint);

	int np, p;
	TCross_Parallel_v1_Parameters parameters;

	parameters.tolerance = 1e-6;
	parameters.maximal_iterations_number = 0;

	MPI_Comm_size(MPI_COMM_WORLD, &np);
	TKernel kernel(full_N, full_N);

	MPI_Comm_rank(MPI_COMM_WORLD, &p);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

	TCross_Parallel_v1 crossed_kernel;
	crossed_kernel.Approximate(&kernel, parameters);

	double *n_k, *n_k_half, *n_k_one, *S;
	double *L1_res_vec, *L2_res_vec;
	double *loc_dots, loc_dot, dot;
	n_k = (double *) malloc (full_N / np * sizeof(double));	
	n_k_half = (double *) malloc (full_N / np * sizeof(double));
	n_k_one = (double *) malloc (full_N / np * sizeof(double));

	loc_dots = (double *) malloc (np * sizeof(double));
	loc_dot = 0;
	loc_dots[p] = 0;
	double start, end;

	start = MPI_Wtime();

	for (int i = 0; i < full_N / np; i++)
		n_k[i] = start_cond(i + p * full_N / np);

	for (int k = 0; k < steps; k++)
	{
		L1_res_vec = crossed_kernel.smol_conv_discrete(n_k);
		L2_res_vec = crossed_kernel.matvec(n_k);
		
		loc_dot = 0;
		dot = 0;
		for ( int l = 0; l < full_N / np; l++)
			loc_dot = loc_dot + (l + 1 + p * full_N / np) * n_k[l] * L2_res_vec[l]; // * lambda * time_step * 0.5;
		
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Allreduce(&loc_dot, &dot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		// predictor step
		if ( p == 0)
			printf("dot = %lf \n", dot);
		for (int i = 0; i < full_N / np; i++)
		{
			n_k_half[i] = ( 0.5 * L1_res_vec[i] - ( 1.0 + lambda) * n_k[i] * L2_res_vec[i] ) * 0.5 * time_step + n_k[i];

			if ( (p == 0) && (i == 0))
				n_k_half[i] += dot * lambda * 0.5 * time_step;

			if (n_k_half[i] < 0)
				n_k_half[i] = 0.0;
		}
		MPI_Barrier(MPI_COMM_WORLD);
		free(L1_res_vec);
		L1_res_vec = NULL;
		free(L2_res_vec);
		L2_res_vec = NULL;
//================================================================================================
//================================================================================================
		//printf("step %d corrector\n", k);
		L1_res_vec = crossed_kernel.smol_conv_discrete(n_k_half);
		
		MPI_Barrier(MPI_COMM_WORLD);	
		L2_res_vec = crossed_kernel.matvec(n_k_half);

		loc_dot = 0;
		dot = 0;
		for ( int l = 0; l < full_N / np; l++)
			loc_dot = loc_dot + (l + 1 + p * full_N / np) * n_k_half[l] * L2_res_vec[l]; // * time_step * lambda;

		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Allreduce(&loc_dot, &dot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

		for(int i = 0; i < full_N / np; i++)
		{

			n_k_one[i] = ( 0.5 * L1_res_vec[i] - ( 1.0 + lambda) * n_k_half[i] * L2_res_vec[i] ) * time_step + n_k[i];

			if ( (p == 0) && (i == 0) )
				n_k_one[0] += dot * lambda * time_step;

			if (n_k_one[i] < 0)
				n_k_one[i] = 0.0;
		}

		MPI_Barrier(MPI_COMM_WORLD);	
		double *tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;
		MPI_Barrier(MPI_COMM_WORLD);	

		get_full_mass(n_k, full_N);
		get_full_density(n_k, full_N);
		free(L1_res_vec);
		L1_res_vec = NULL;
		free(L2_res_vec);
		L2_res_vec = NULL;
		MPI_Barrier(MPI_COMM_WORLD);	
	}

	end = MPI_Wtime();
	if ( p == 0)
	{
		double r_time = end - start;
		printf("rank =%d \n", crossed_kernel.get_rank());
		printf("runtime = %lf \n", r_time);
	}
	MPI_Finalize();
	return 0.0;
}
