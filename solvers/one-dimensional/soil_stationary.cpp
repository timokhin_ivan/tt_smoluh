#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"
#include <sys/time.h>
using namespace std;
//=================================================================
//=================================================================
// GLOBAL PARAMETERS OF THE AGGREGATION MODEL;!placed at input file
//=================================================================
//=================================================================
double GAMMA  = 1e-1;
int N1        = 80;
double alpha  = 2.0;
double beta   = 0.5;
double aggr_C = 0.1;
//=================================================================
//=================================================================
// FUNCTIONS FOR AGGREGATION AND FRAGMENTATION KERNELS
//=================================================================
//=================================================================
double K_a(const int & u, const int &v)
{
//	return 1.0;
	double u1=u + 1.0;
	double v1=v + 1.0;
	return aggr_C * pow(0.75 / M_PI, 2.0l / 3) * pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), alpha) * pow(8.0l /M_PI * (1.0l / u1 + 1.0l / v1), beta);
}
//=================================================================
//=================================================================
double K_f (const int &i, const double gamma, const int n1)
{
    double u = u + 1.0;
    if (u < (double) n1)
        return 1.0;
    else
        return exp(gamma * (u - n1));

}
//=================================================================
//=================================================================
// AGGREGATION KERNEL CLASS DEFINITION
//=================================================================
//=================================================================
class TKernel: public TMatrix {
        public:
		int kernel_type ;
                TKernel (const int &, const int &);
                double value (const int & , const int &);

};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
}
//=================================================================
//=================================================================
double TKernel::value(const int &i, const int &j)
{
	if (this -> kernel_type == 1)
	        return  K_a(i , j);
	if (this -> kernel_type == 2)
		return K_a(i + 1, j + 1);
	else
		return K_a(i, j);
}
//=================================================================
//=================================================================
// WORK-ON-FLY SUBROUTINES
//=================================================================
//=================================================================
double *Fragmentation_term(const int &N, const double *n)
{
    double *res = new double[N];
    for (int i = 0; i < N; i++)
        res[i] = 0.0;
    
    // res[N-1] = 0.0
    for (int i = N - 2; i >= 0; i--)
    {
//        res[i] = res[i + 1] + K_f(i + 1, GAMMA, N1) * (i + 2) / (i + 1) * n[i + 1]; // mass fragmentation
        res[i] = res[i + 1] + K_f(i + 1, GAMMA, N1) / (i + 1) * n[i + 1];             // density fragmentation
    }
    return res;
}
//=================================================================
//=================================================================
double Total_mass(const int &N, const double *n)
{
    double l2 = 0;
    for (int i = 0; i < N; i++)
    {
        l2 += n[i] * (i + 1);
    }
    return l2;
}
//=================================================================
//=================================================================
double Cluster_density(const int &N, const double *n)
{
    double l2 = 0;
    for (int i = 0; i < N; i++)
    {
        l2 += n[i];
    }
    return l2;
}
//=================================================================
//=================================================================
double Starting_condition(const int &v)
{
//	return 0.0;
/*	if ( (v < 100)  )
		return 1.0/ 5050.0;
	else
		return 0.0;*/
        return exp(-0.1 * v) / 3.0;
}
//=================================================================
//=================================================================
double *Smoluchowski_operator(double *n_inp, int N, TCross_Parallel_v1 &crossed_kernel)
{
        double *convol_res_vec;
        double *aggregation_matvec;
        double *fragmentation_term;
        double *n_out = new double[N];
        convol_res_vec = crossed_kernel.smol_conv_discrete(n_inp);// compute convol_res_vec[i]     = \sum_{j=1}^{i-1} K_a(i - j, j) * n(i - j,t) * n(j,t)
        aggregation_matvec = crossed_kernel.matvec(n_inp, 'q');   // compute aggregation_matvec[i] = \sum{j=1}^{N - i + 1} K_a(j, i) * n(j, t)
        fragmentation_term = Fragmentation_term(N, n_inp);        // compute fragmentation term[i] = \sum_{k=i+1}^N K_f(k) * n(k, t) * k / ( k - 1)     <-- must be multiplied elementwise by 1/i
        //for (i = 0; i < N; i++)
        //{
        //    workspace_for_source[i] = (i + 1) * n_k[i];
        //}
        /*mono_vec    = crossed_kernel.matvec(n_k, 'p');
        mono_source = 0.0;
        for (i = 1; i < N; i++)
        {
            mono_source += mono_vec[i] * (i + 1) * n_k[i];
        }
        mono_source = 2.0 * mono_source / (N1 * (N1 + 1));*/
        // fix into the explicit formulas and provide the time-integration step
        for (int i = 0; i < N; i++)
        {
            //fragmentation_term[i] = fragmentation_term[i] / (i + 1.0);         //   fragmentation term[i] = \sum_{k=i+1}^N  K_f(k) * n(k, t) * k / ((k - 1) * i )
            fragmentation_term[i] = fragmentation_term[i] * 2.0;                 //   fragmentation term[i] = \sum_{k=i+1}^N  K_f(k) * n(k, t) * 2 / (k - 1))
            //printf("fragmentation term[%d] = %lf\n", i, fragmentation_term[i]);//   DEBUG PRINT
            //printf("convolution term[%d] = %lf\n", i, convol_res_vec[i]);      //   DEBUG PRINT
            //if (i > 0)
                n_out[i] = ( 0.5 * convol_res_vec[i] - n_inp[i] * aggregation_matvec[i] - K_f(i, GAMMA, N1) * n_inp[i] + fragmentation_term[i]);
            //else
            //    n_k_half[i] = n_k[i] + 0.5 * tau * ( 0.5 * convol_res_vec[i] - n_k[i] * aggregation_matvec[i] + fragmentation_term[i]);
            /*if (i < N1)
                n_k_half[i] += 0.5 * tau * mono_source;*/
        }        
        //free(mono_vec);
        delete [] convol_res_vec;
        delete [] aggregation_matvec;
        delete [] fragmentation_term;
        return n_out;
}
//=================================================================
//=================================================================
// MAIN FUNCTION SOURCE CODE
//=================================================================
//=================================================================
int main(int argc, char** argv)
{
    int i, j ,k;
    char command_string[1000];
    char values[430];
    FILE *values_file;

    double tolerance = 1e-6; // cross interpolation tolerance
    double eps_iter = 1e-8;       // time integration step
    double eps;
    int N_iter = 200;       // N time integration steps
    int N = 5000;            // N_equtions
    int if_print_out = 0;    // if print results to STDOUT
    int if_paint     = 0;    // if save the snapshots with the dynamics
    char datadir[200];       // firectory to save the data
    
    FILE *mass_dynamics;            //
    char mass_filename[250];        //
    FILE *density_dynamics;         //
    char density_filename[250];     //
    FILE *convergence_dynamics;     //
    char convergence_filename[250]; //
//=================================================================
//=================================================================
// READ THE PROGRAM PARAMETERS FROM THE INPUT FILE soil_stationary.inp
//=================================================================
//=================================================================
// N            -- number of equations, integer
// eps_iter     -- tolerance parameter of the fixed point iteration methos, float
// tolerance    -- tolerance of cross interpolation method, float
// N_iter       -- maximal number of iterations
// N1           -- size of the aggregates from which the fragmentation process starts
// GAMMA        -- parameter of the exponent in the fragmentation kernel
// aggr_C       -- parameter of the aggregation kernel
// alpha        -- parameter of the aggregation kernel
// beta         -- parameter of the aggregation kernel
// if_print_out -- if program prints results to STDOUT
// if_paint     -- if program paints snapshots
// datadir      -- name of the directory to save the data 
    FILE *program_params = fopen("soil_stationary.inp", "r");
    
    fscanf(program_params, "%d\n",  &N);                 // N_equations
    fscanf(program_params, "%lf\n", &eps_iter);          // time integration step
    fscanf(program_params, "%lf\n", &tolerance);         // CROSS tolerance parameter
    fscanf(program_params, "%d\n",  &N_iter);            // number of time integration steps
    fscanf(program_params, "%d\n",  &N1);                // N1 is the minimal size of the particles which can be destroyed by fragmentation
    fscanf(program_params, "%lf\n", &GAMMA);             // GAMMA is the parameter of the exponent in the fragmentation kernel
    fscanf(program_params, "%lf\n", &aggr_C);            // aggr_C is the parameter of the aggregation kernel
    fscanf(program_params, "%lf\n", &alpha);             // alpha is the parameter of the aggregation kernel
    fscanf(program_params, "%lf\n", &beta);              // beta is the parameter of the aggregation kernel
    fscanf(program_params, "%d\n",  &if_print_out);      // if print results to STDOUT
    fscanf(program_params, "%d\n",  &if_paint);          // if paint snapshots
    fscanf(program_params, "%s\n",  datadir);            // DATA directory to save the information about the performed computations    
//=================================================================
//================================================================= 
// create the data directory and the appropriate paths
    sprintf(command_string, "mkdir %s", datadir);
    system(command_string);
    sprintf(command_string, "cp plot_soil %s", datadir);
    system(command_string);
    sprintf(values, "%s/values.dat", datadir);
    sprintf(density_filename, "%s/density_dynamics.dat", datadir);
    sprintf(mass_filename, "%s/mass_dynamics.dat", datadir);
    sprintf(convergence_filename, "%s/convergence.dat", datadir);
//=================================================================
//================================================================= 
// setup aggregation kernel 
    TKernel kernel_a(N, N);
    kernel_a.kernel_type = 1;
    TCross_Parallel_v1_Parameters parameters;
    parameters.tolerance = tolerance;
    parameters.maximal_iterations_number = 0;
    TCross_Parallel_v1 crossed_kernel;
    crossed_kernel.Approximate(&kernel_a, parameters);    
//=================================================================
//=================================================================
    double *n_k      = new double[N];
    double *S        = new double[N];
    double *n_k_one  = new double[N];
    double *Smoluchowski_operator_result;
    double *bubble;    
    
    for (i = 0; i < N; i++)
    {
        n_k[i] = Starting_condition(i);     
    }
    
    double cluster_density, total_mass;
    
    cluster_density = Cluster_density(N, n_k);
    total_mass      = Total_mass(N, n_k);
    
    struct timeval start, end;
    double r_time = 0.0;
    gettimeofday(&start, NULL);
    if (if_print_out)
    {
        printf("Starting Total density = %lf\n", cluster_density);
        printf("Starting Total mass = %lf\n", total_mass);
    }
    // create the new files with the starting mass and density
    mass_dynamics = fopen(mass_filename, "w+");
    fprintf(mass_dynamics, "%d %E\n", (k + 1) , total_mass); // in terms of mass concentration
    fclose(mass_dynamics);
            
    density_dynamics = fopen(density_filename, "w+");
    fprintf(density_dynamics, "%d %E\n", (k + 1), cluster_density);
    fclose(density_dynamics);
//=================================================================
//=================================================================
// START OF THE FIXED POINT ITERATIONS
//=================================================================
//=================================================================
    k = 0;
    do
    {
        //=================================================================
        //=================================================================
        // fixed point iteration
        //=================================================================
        //=================================================================
        Smoluchowski_operator_result = Smoluchowski_operator(n_k, N, crossed_kernel);
        for (i = 0; i < N; i++)
        {            
            n_k_one[i] = n_k[i] + Smoluchowski_operator_result[i];
            if (n_k_one[i] < 0.0)
                n_k_one[i] = 0.0;
            S[i]       = n_k_one[i];
        }        
        delete [] Smoluchowski_operator_result;
        
        cluster_density = Cluster_density(N, n_k_one);
        total_mass      = Total_mass(N, n_k_one);
        // check the convergence
        cblas_daxpy(N, -1.0, n_k, 1, S, 1);
        eps = cblas_dnrm2(N, S, 1);
        
        
        if (if_print_out)
        {
            printf("iter = %d from %d, eps = %E\n", k + 1, N_iter, eps);
            printf("iter %d, Total density = %lf\n", k + 1, cluster_density);
            printf("iter %d, Total mass = %lf\n", k + 1, total_mass);
        }
        
        //=================================================================
        //=================================================================
        // save the data and paint snapshots
        //=================================================================
        //=================================================================
        // save the dynamics of mass and the total density
        mass_dynamics = fopen(mass_filename, "a+");
        fprintf(mass_dynamics, "%d %E\n", k + 1, total_mass);
        fclose(mass_dynamics);
        
        density_dynamics = fopen(density_filename, "a+");
        fprintf(density_dynamics, "%d %E\n", k + 1, cluster_density);
        fclose(density_dynamics);
        
        convergence_dynamics = fopen(convergence_filename, "a+");
        fprintf(convergence_dynamics, "%d %E\n", k + 1, eps);
        fclose(convergence_dynamics);
        
        if (if_paint && ((N_iter < 1000) || (k % 1000 == 0) || (k == 0) ))
        {
            // print data to file and call gnuplot to create the snapshot
            values_file = fopen(values, "w+");
            for ( i = 0; i < N; i++)
                fprintf(values_file, "%d %E\n", i + 1, (i + 1) * n_k_one[i]); // in terms of mass concentration
            
            fclose(values_file);
            sprintf(command_string, "cd %s; gnuplot plot_soil; cd ../", datadir);
            system(command_string);
            
            if (N_iter < 1000)
                sprintf(command_string, "mv %s/1.png %s/file%04d.png", datadir, datadir, k + 1);
            else if (k % 1000 == 0)
                sprintf(command_string, "mv %s/1.png %s/file%04d.png", datadir, datadir, (k + 1)/1000);
            
            system(command_string);
        }
        // replace the arrays before the new step occases
        bubble = n_k_one;
        n_k_one = n_k;
        n_k = bubble;
        bubble = NULL;
        k++;
    } while ((k < N_iter) && (eps > eps_iter));
    gettimeofday(&end, NULL);
    r_time = end.tv_sec - start.tv_sec + ((double) (end.tv_usec - start.tv_usec)) / 1000000;
//=================================================================
//=================================================================
// SAVE THE FINAL DATA AND THE INFORMATION ABOUT THE COMPUTATIONS  
//=================================================================
//================================================================= 
    char final_data_file[300];
    sprintf(final_data_file, "%s/final.dat", datadir);
    FILE *final_data = fopen(final_data_file, "w+");
    // save the final snapshot with the solution into file
    for (int i = 0; i < N; i++)
        fprintf(final_data, "%d %E\n", i + 1, (i + 1) * n_k_one[i]);// in terms of mass concentration
    fclose(final_data);
    // print the program parameters into log file
    char log_file[300];
    sprintf(log_file, "%s/info.log", datadir);
    FILE * log = fopen(log_file, "w+");
    fprintf(log, "N_equations %d\n", N);
    fprintf(log, "GAMMA %lf\n", GAMMA);
    fprintf(log, "alpha %lf\n", alpha);
    fprintf(log, "beta %lf\n", beta);
    fprintf(log, "N1, %d\n", N1);
    fprintf(log, "N_iter = %d\n", N_iter);
    fprintf(log, "eps = %lf\n", eps);
    fprintf(log, "========================================\n");
    fprintf(log, "========================================\n");
    fprintf(log, "runtime = %lf\n", r_time);
    fprintf(log, "rank = %d\n", crossed_kernel.get_rank());
    fclose(log);
//=================================================================
//=================================================================
// deallocate memory
    delete [] n_k;
    delete [] n_k_one;
    delete [] S;
    return 0;
}
