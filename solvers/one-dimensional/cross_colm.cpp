#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"
#include <sys/time.h>
using namespace std;

double K(const int & u, const int &v)
{
//	return 1.0;
/*	if (u == 0)
		return K(1, v);
	if (v == 0)
		return K(u, 1);*/
	return pow(u + 1.0, 0.75l) * pow(v + 1.0, -0.75l) + pow (u + 1.0, -0.75l) * pow (v + 1.0, 0.75l);
	//	return M_PI * pow(0.75 / M_PI, 2.0l/3) * pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt(8.0l /M_PI * (1.0l / u1 + 1.0l / v1));
}

class TKernel: public TMatrix {
	public:
		TKernel (const int &m, const int &n): TMatrix(m,n)
		{
			
		}
		double value (const int &i , const int &j)
		{
			return K(i,j);
		}
};
double L1(const int &N, const int &i, const double *n)
{
	double l1 = 0;
	for (int i1 = 1; i1 <= i; i1++)
	{
		l1 += n[i1 - 1] * n[i - i1] * K(i1, i - i1 + 1);
	}
	return l1;
}
double L2(const int &N, const int &i, const double *n)
{
	double l2 = 0;
	for (int i1 = 0; i1 < N; i1++)
	{
		l2 += K(i, i1) * n[i1]  ;
	}
	l2 *= n[i];
	return l2;
}
double L_for_vol(const int &N, const double *n)
{
	double l_for_vol = 0;
	for (int i = 0; i < N; i++)
	{
		l_for_vol += (i + 1) * n[i];
	}
	return l_for_vol;
}
double start_cond(const double &v)
{
	return 0.0;
}
double analitycal_solution(const double &v, const double & t)
{
	return (1.0 / ((1.0 + t / 2.0) * (1.0 + t / 2.0))) * exp(-v / (1.0 + t / 2.0));
}

int main(int argc, char** argv)
{
        if (argc != 7)
        {
                printf("6 parameters have been expected\n");
                printf("N n_steps time_step tol J ifpaint \n");
                return -1;
        }
	char if_with_slow = 0;
        double v_max = 10.0;
        int N = 101, steps = 100;
        double time_step = 0.01;
        char name_of_file[15] = "values.dat";
        char name_of_picture[30];
        double tolerance = 1e-6;
        FILE* values, *mass_dynamics;
	int ifpaint = 0;
	double J = 1.0;
        TCross_Parallel_v1_Parameters parameters;
	double t = 0.0;
        sscanf(argv[1], "%d", &N);
        sscanf(argv[2], "%d", &steps);
        sscanf(argv[3], "%lf", &time_step);
        sscanf(argv[4], "%lf", &tolerance);
	sscanf(argv[5], "%lf", &J);
	sscanf(argv[6], "%d", &ifpaint);
	printf("time step  = %lf\n", time_step);


        if (ifpaint)
	        system("cd IMG; rm *.png; cd ..");

//	for fast algorithm
        double *n_k, *n_k_half, *n_k_one, *S;
        double *L1_res_vec, *L2_res_vec, *L1_correction, *L2_correction;

        n_k = (double *) malloc (N * sizeof(double));
        n_k_half = (double *) malloc (N * sizeof(double));
        n_k_one = (double *) malloc (N * sizeof(double));
        S = (double *) malloc (N * sizeof(double));

	L1_correction = (double *)malloc (N * sizeof(double));
	L2_correction = (double *)malloc (N * sizeof(double));

       	TKernel kernel(N, N);
        parameters.tolerance=tolerance;
        parameters.maximal_iterations_number = 0;
        
        TCross_Parallel_v1 crossed_kernel;
        crossed_kernel.Approximate(&kernel, parameters);
	struct timeval start, end;
	double r_time = 0.0;
//	for slow algorithm
	double *n_k_direct, *n_k_half_direct, *n_k_one_direct;
	double *difference = (double *) malloc (N * sizeof(double));

	n_k_direct = (double *) malloc(N * sizeof(double));
	n_k_half_direct = (double *) malloc (N * sizeof(double));
	n_k_one_direct = (double *) malloc (N * sizeof(double));
	struct timeval start_direct, end_direct;
	double r_time_direct = 0.0;
//	starting conditions
        for (int i = 0; i < N; i++)
        {
                n_k[i] = start_cond(i);
		n_k_direct[i] = start_cond(i);
        }
//	start scheme
        for (int k = 0; k < steps; k++)
        {
// ================================================================================================
// ================================================================================================
// 		fast computations of L1 and L2 
                L2_res_vec=crossed_kernel.matvec(n_k);
                L1_res_vec=crossed_kernel.smol_conv_discrete(n_k);
//		predictor step
		for (int i = 0; i < N; i++)
		{
			//			slow
			if (if_with_slow)
				n_k_half_direct[i] = ( L1(N, i, n_k_direct) * 0.5 - n_k_direct[i] * L2(N, i, n_k_direct) ) * 0.5 * time_step + n_k_direct[i];
			//			fast
			if (i > 0)
				n_k_half[i] = ( L1_res_vec[i] * 0.5 - n_k[i] * L2_res_vec[i]  )  * 0.5 * time_step + n_k[i] ;
			else
				n_k_half[i] = ( L1_res_vec[i] * 0.5 - n_k[i] * L2_res_vec[i] + J )  * 0.5 * time_step + n_k[i] ;
			if (n_k_half[i] < 0 )
				n_k_half[i] = 0.0;
		}
		if(if_with_slow)
			n_k_half_direct[0] += 0.5 * J;

                free (L2_res_vec);
                L2_res_vec=NULL;
                free (L1_res_vec);
                L1_res_vec=NULL;
// ================================================================================================
// ================================================================================================
//		printf("Step %d corrector\n", k+1);
		if (ifpaint && ( (steps < 1000) || (k % 100 == 0) ))
	                values=fopen (name_of_file, "w+");
//		fast computations of L1 and L2
                L2_res_vec=crossed_kernel.matvec(n_k_half);
                L1_res_vec=crossed_kernel.smol_conv_discrete(n_k_half);
//		corrector step
                for (int i = 0; i < N; i++)
                {
//			slow
			if(if_with_slow)
				n_k_one_direct[i] = (L1(N, i, n_k_half_direct) * 0.5 - n_k_half_direct[i] * L2(N, i, n_k_half_direct)) * time_step + n_k_direct[i];
//			fast
			if ( i > 0)
			{
				S[i] = (L1_res_vec[i] * 0.5 - n_k_half[i] *  L2_res_vec[i] ) ;
				n_k_one[i] = S[i] * time_step + n_k[i];
			}
			else
			{
				S[i] = (L1_res_vec[i] * 0.5 - n_k_half[i] *  L2_res_vec[i] + J ) ;
				n_k_one[i] = S[i] * time_step + n_k[i];
			}

			if(n_k_one[i] < 0.0 )
				n_k_one[i] = 0.0;

			if (ifpaint && ( (steps < 1000) || (k % 100 == 0)))
                        	fprintf(values, "%lf %lf \n", (i + 1.0), (i + 1.0) * n_k_one[i]);
                }
                free (L1_res_vec);
                L1_res_vec=NULL;
                free (L2_res_vec);
                L2_res_vec=NULL;
//		paint pictures
		if (ifpaint)
		{
			if (steps < 1000)
			{
				fclose(values);
				system("gnuplot plot_cross_colm");
				sprintf(name_of_picture, "mv 1.png IMG/file%03d.png",k+1);
				system(name_of_picture);
				//printf("<saving image data about the step...\n");

			}
			else if (k % 100 == 0)
			{
				fclose(values);
				system("gnuplot plot_cross_colm");
				sprintf(name_of_picture, "mv 1.png IMG/file%04d.png",(k + 1) / 100);
				system(name_of_picture);
				//printf("<saving image data about the step...\n");

			}
		}
//		check volume, error, difference between fast and slow

		if ( (steps < 1000) || (k % 100 == 0) )
		{
			double volume = L_for_vol(N, n_k_one);
			mass_dynamics = fopen ("mass_dynamics.dat", "a+");
			fprintf (mass_dynamics, "%lf %lf \n", k * time_step, volume);
			fclose(mass_dynamics);
		}

		if(if_with_slow)
		{
			n_k_one_direct[0] += time_step * J;
			for (int i = 0; i < N; i ++ )
				difference[i] = n_k_one_direct[i] ;

			cblas_daxpy(N, -1.0, n_k_one, 1, difference, 1);

			double diff_norm = cblas_dnrm2(N, difference, 1);
			printf("difference norm2 = %E \n", diff_norm);
			printf("relative difference norm = %E \n", diff_norm / cblas_dnrm2(N, n_k_one, 1));

			double *tmp_direct = n_k_one_direct;
			n_k_one_direct = n_k_direct;
			n_k_direct = tmp_direct;
			tmp_direct = NULL;
		}

		double * tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;

        }

	FILE *final_data = fopen("final.dat", "w+");

	for (int i = 0; i < N; i++)
		fprintf(final_data, "%d %E\n", i+1, n_k_one[i]);
	fclose(final_data);

	/*printf("final time = %lf\n", steps * time_step );
	printf("N_steps = %d\n", steps);
	printf("Kernel rank = %d \n", crossed_kernel.get_rank());
	*/
        free(n_k);
        free(n_k_half);
        free(n_k_one);

        free(n_k_direct);
        free(n_k_half_direct);
        free(n_k_one_direct);

	free(difference);
//	fclose(mass_dynamics);
}
