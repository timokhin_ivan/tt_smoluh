#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include "../../libtt/tensor_train.h"
#include <sys/time.h>
using namespace std;

double K(const int & u, const int &v)
{
	return 1.0;
	double u1=u+1.0;
	double v1=v+1.0;
//	return pow(u1*v1, 1.0l/8.0l);
//	return pow(u1, 1.0l) * pow(v1, -1.0l) + pow (u1, -1.0l) * pow (v1, 1.0l);
//	return M_PI * pow(0.75 / M_PI, 2.0l/3) * pow(pow(u1, 1.0l / 3) + pow(v1, 1.0l / 3), 2) * sqrt(8.0l /M_PI * (1.0l / u1 + 1.0l / v1));
}

class TKernel: public TMatrix {
        public:
		int kernel_type ;
                TKernel (const int &, const int &);
                double value (const int & , const int &);

};

TKernel::TKernel(const int &m, const int &n): TMatrix (m, n)
{
}

double TKernel::value(const int &i, const int &j)
{
	if (this -> kernel_type == 1)
	        return  K(i , j);
	if (this -> kernel_type == 2)
		return K(i + 1, j + 1);
	else
		return K(i, j);
}


double L_for_vol(const int &N, const double *n)
{
    double l2 = 0;
    for (int i = 0; i < N; i++)
    {
        l2 += n[i] * (i + 1);
    }
    return l2;
}

double cluster_density(const int &N, const double *n)
{
    double l2 = 0;
    for (int i = 0; i < N; i++)
    {
        l2 += n[i];
    }
    return l2;
}

double analitycal_density(double lambda, double t)
{
	if (lambda == 0)
		return (2 / (2 + t));
	else
		return ( 2 * lambda) / (1.0 + 2.0 * lambda - exp (- lambda * t));
}

double start_cond(const int &v)
{
	return 0.0;
	if ( (v < 10)  )
		return 1.0/10.0;
	else
		return 0.0;
}

double sources(const int &v)
{
    if (v == 0)
        return 1e-3;
    if (v == 99)
        return 1e-5;
    return 0.0;
}

double analitycal_solution(const double &v, const double & t)
{
    return (1.0 / ((1.0 + t / 2.0) * (1.0 + t / 2.0))) * exp(-v / (1.0 + t / 2.0));
}

int main(int argc, char** argv)
{
        if (argc != 8)
        {
                printf("7 parameters have been expected\n");
                printf("n_steps time_step n_equations tol lambda datadir ifpaint\n");
                return -1;
        }
        int N = 101, steps = 100, ifpaint = 0;
        double time_step = 0.01, lambda = 1e-2, tolerance = 1e-6;

	char datadir[100]="IMG", command_string[500];
        FILE* values, *mass_dynamics, *density_dynamics;
        char name_of_file[200], mass_dynamics_file[200], density_dynamics_file[200];
	double J = 0.0;//source

        sscanf(argv[1], "%d", &steps);
        sscanf(argv[2], "%lf", &time_step);
        sscanf(argv[3], "%d", &N);
        sscanf(argv[4], "%lf", &tolerance);
	sscanf(argv[5], "%lf", &lambda);
	sscanf(argv[6], "%s", datadir);
	sscanf(argv[7], "%d", &ifpaint);

	sprintf(command_string, "mkdir %s", datadir);
	system(command_string);
	sprintf(name_of_file, "%s/values.dat", datadir);
	sprintf(mass_dynamics_file, "%s/mass_dynamics.dat", datadir);
	sprintf(density_dynamics_file, "%s/density_dynamics.dat", datadir);

	sprintf(command_string, "cp plot_cross_IBRAE %s/", datadir);
	system(command_string);

	sprintf(command_string, "cd %s; rm *.png; cd ../", datadir);
        if (ifpaint)
	        system(command_string);
//	for fast algorithm
        double *n_k, *n_k_half, *n_k_one, *n_analit, *S;
        double *L1_res_vec, *L2_res_vec;
	double *workspace_for_mono, *L3_for_mono_vec, *workspace_for_source;

        n_k = (double *) malloc (N * sizeof(double));
        n_k_half = (double *) malloc (N * sizeof(double));
        n_k_one = (double *) malloc (N * sizeof(double));
        n_analit = (double *) malloc (N * sizeof(double));
        S = (double *) malloc (N * sizeof(double));

	workspace_for_mono = (double *) malloc ((N - 1) * sizeof(double));
	double L3_for_mono, L4_for_mono;

        TCross_Parallel_v1_Parameters parameters;
        parameters.tolerance=tolerance;
        parameters.maximal_iterations_number = 0;

       	TKernel kernel(N, N);
	kernel.kernel_type = 1; 
        TCross_Parallel_v1 crossed_kernel;
        crossed_kernel.Approximate(&kernel, parameters);

	TKernel monomers_kernel(N - 1, N - 1);
	monomers_kernel.kernel_type = 2;
	TCross_Parallel_v1 crossed_mono_kernel;
	crossed_mono_kernel.Approximate(&monomers_kernel, parameters);

	struct timeval start, end;
	double r_time = 0.0;
//	starting conditions
        for (int i = 0; i < N; i++)
		n_k[i] = start_cond(i);
        
	double starting_vol = L_for_vol(N, n_k);
	double starting_density = cluster_density(N, n_k);
	gettimeofday(&start, NULL);
//	start scheme
        for (int k = 0; k < steps; k++)
        {
// ================================================================================================
// ================================================================================================
// 		fast computations of L1, L2, L3 and L4
                L1_res_vec = crossed_kernel.smol_conv_discrete(n_k);
                L2_res_vec = crossed_kernel.matvec(n_k);
		L3_for_mono = 0.0;
		L4_for_mono = 0.0;
		for (int i = 1; i < N; i++)
		{
			workspace_for_mono[i - 1] = (i + 1) * n_k [i];
			L4_for_mono += K(0, i) * n_k[i] * (i + 1); 
		}

		L3_for_mono_vec = crossed_mono_kernel.matvec(workspace_for_mono);
		for (int i = 1; i< N; i++)
			L3_for_mono += n_k[i] * L3_for_mono_vec[i - 1];

		L4_for_mono *= lambda * n_k[0];
		
		workspace_for_source = crossed_kernel.matvec(n_k, 'p');
		J = 0.0;
		for (int i = 0; i < N; i++)
			J += workspace_for_source[i] * n_k[i] * (i + 1);
                // nullify mass losses -- sink of too big particles
                J = 0.0;
//		predictor step
                for (int i = 0; i < N; i++)
                {
//			fast
			if (i > 0)
	                        n_k_half[i] = ( L1_res_vec[i] * 0.5 - (1.0 + lambda) * n_k[i] * L2_res_vec[i] + sources(i)) * 0.5 * time_step + n_k[i];
			else
	                        n_k_half[i] = ( - n_k[i] * L2_res_vec[i] + L3_for_mono * lambda  + L4_for_mono + sources(i) + J) * 0.5 * time_step + n_k[i] ;
			if (n_k_half[i] < 0.0)
				n_k_half[i] = 0.0;
                }
                free (L1_res_vec);
                L1_res_vec = NULL;
                free (L2_res_vec);
                L2_res_vec = NULL;
		free(L3_for_mono_vec);
		L3_for_mono_vec = NULL;
		free(workspace_for_source);
		workspace_for_source = NULL;
//		check error
                double volume = L_for_vol(N, n_k);
// ================================================================================================
// ================================================================================================
// ================================================================================================
// ================================================================================================
		if (ifpaint && ( ( steps < 1000 ) || (k % 1000 == 0) ) )
	                values=fopen (name_of_file, "w+");
//		fast computations of L1 and L2
                L1_res_vec = crossed_kernel.smol_conv_discrete(n_k_half);
                L2_res_vec=crossed_kernel.matvec(n_k_half);
		L3_for_mono = 0.0;
		L4_for_mono = 0.0;

		for (int i = 1; i < N; i++)
		{
			workspace_for_mono[i - 1] = (i + 1) * n_k_half [i];
			L4_for_mono += K(0, i) * n_k_half[i] * (i + 1);
		}

		L3_for_mono_vec = crossed_mono_kernel.matvec(workspace_for_mono);
		for (int i = 1; i< N; i++)
			L3_for_mono += n_k_half[i] * L3_for_mono_vec[i - 1];
		
		L4_for_mono *= lambda * n_k_half[0];
		
		workspace_for_source = crossed_kernel.matvec(n_k_half, 'p');
		J = 0.0;
		for (int i = 0; i < N; i++)
			J += workspace_for_source[i] * n_k_half[i] * (i + 1);
                // nullify mass losses -- sink of too big particles
                J = 0.0;
//		corrector step
                for (int i = 0; i < N; i++)
                {
			if (i > 0 )
			{
				S[i] = (L1_res_vec[i] * 0.5 - (1.0 + lambda) * n_k_half[i] *  L2_res_vec[i] + sources(i));
	                	n_k_one[i] = S[i] * time_step + n_k[i];
			}
			else
			{

			    	S[i] = (- n_k_half[i] *  L2_res_vec[i] + L3_for_mono * lambda + L4_for_mono + sources(i) + J);
	                	n_k_one[i] = S[i] * time_step + n_k[i];
				volume = L_for_vol(N, n_k_half);
			}

			if (n_k_one[i] < 0.0)
				n_k_one[i] = 0.0;

			if (ifpaint && ( ( steps < 1000 ) || (k % 1000 == 0) ))
                        	fprintf(values, "%d %E\n", (i + 1), n_k_one[i]);
                }
                free (L1_res_vec);
                L1_res_vec = NULL;
                free (L2_res_vec);
                L2_res_vec = NULL;
		free(L3_for_mono_vec);		
		L3_for_mono_vec = NULL;
		free(workspace_for_source);
		workspace_for_source = NULL;
// ================================================================================================
// ================================================================================================
//		paint pictures
		if (ifpaint && ( ( steps < 1000 ) || (k % 1000 == 0) ))
		{
			fclose(values);
			sprintf(command_string, "cd %s; gnuplot plot_cross_IBRAE;cd ../", datadir);
			system(command_string);
				
			if (steps < 1000)
				sprintf(command_string, "mv %s/1.png %s/file%04d.png", datadir, datadir, k + 1);
			else if (k % 1000 == 0)
				sprintf(command_string, "mv %s/1.png %s/file%04d.png", datadir, datadir, (k + 1) / 1000);
			system(command_string);
		}
//		check volume conservation, error, difference between fast and slow
		volume = L_for_vol(N, n_k_one);
		double clust_dens = cluster_density(N, n_k_one);
		double analit_dens = analitycal_density(lambda, time_step * k);

		if ( ( steps < 1000 ) || (k % 1000 == 0) )
		{
			mass_dynamics = fopen (mass_dynamics_file, "a+");
			fprintf (mass_dynamics, "%lf %lf \n", (k + 1) * time_step, volume);
			fclose(mass_dynamics);

			density_dynamics = fopen (density_dynamics_file, "a+");
			fprintf(density_dynamics, "%lf %lf\n", (k + 1) * time_step, clust_dens);
			fclose(density_dynamics);
		}
		double * tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;

	}
// ================================================================================================
// ================================================================================================
// finish of scheme
	gettimeofday(&end, NULL);
	r_time = end.tv_sec - start.tv_sec + ((double) (end.tv_usec - start.tv_usec)) / 1000000;

	char final_data_file[200];
	sprintf(final_data_file, "%s/final.dat", datadir);
	FILE *final_data = fopen(final_data_file, "w+");

	for (int i = 0; i < N; i++)
		fprintf(final_data, "%d %E\n", i+1, n_k_one[i]);
	fclose(final_data);

	char log_file[200];
	sprintf(log_file, "%s/info.log", datadir);
	FILE * log = fopen(log_file, "w+");
	fprintf(log, "N_equations %d\n", N);
	fprintf(log, "lambda = %lf\n", lambda);
	fprintf(log, "N_steps = %d\n", steps);
	fprintf(log, "time_step = %lf\n", time_step);
	fprintf(log, "========================================\n");
	fprintf(log, "========================================\n");
	fprintf(log, "runtime = %lf\n", r_time);
	fprintf(log, "rank = %d\n", crossed_kernel.get_rank());	
	fclose(log);

        free(n_k);
        free(n_k_half);
        free(n_k_one);
	free(workspace_for_mono);
        free(n_analit);
}
