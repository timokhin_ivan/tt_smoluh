#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <mkl.h>
#include <cmath>
#include <cstring>
#include <sys/time.h>
using namespace std;

double K(const double & u, const double &v, const double h)
{
	return 1.0;

/*	if (u == 0 && v == 0)
		return K(0.5 , 0.5  , h);
//		return pow(0.5*h, 0.95l)*pow(0.5*h, -0.95l) + pow (0.5*h, -0.95l)* pow (0.5 * h, 0.95l);
	if (u == 0)
		return K(0.5 , v, h);
//		return pow(0.5*h, 0.95l)*pow(v*h, -0.95l) + pow (0.5*h, -0.95l)* pow (v*h, 0.95l);
	if (v == 0)
		return K(u , 0.5 , h);*/
//		return pow(u*h, 0.95l)*pow(0.5*h, -0.95l) + pow (u*h, -0.95l)* pow (0.5*h, 0.95l);

//	if (u==0||v==0)
//		return 0.0;

//	return pow(u*h, 0.95l)*pow(v*h, -0.95l) + pow (u*h, -0.95l)* pow (v*h, 0.95l);
	if (u==0||v==0)
		return 0.0;
	double u1 = u * h;
	double v1 = v * h;

//	return M_PI*pow(0.75/M_PI,2.0l/3)*pow(pow(u1,1.0l/3)+pow(v1, 1.0l/3),2)*sqrt(8.0l/M_PI * (1.0l/u1 + 1.0l/v1));
	return pow(pow(u1,1.0l/3)+pow(v1, 1.0l/3),2)*sqrt((1.0l/u1 + 1.0l/v1));
}


double L1(const int &N, const int &i, const double *n, const double &h)
{
    double l1 = 0;
    for (int i1 = 1; i1 <= i; i1++)
    {
        l1 += n[i1] * n[i - i1] * K((i - i1) , i1 ,h) + n[i1 - 1] * n[i - i1 + 1] * K((i - i1 + 1) , (i1 - 1) ,h);
    }
    l1 *= h/2;
    return l1;
}




double L2(const int &N, const int &i, const double *n, const double &h)
{
    double l2 = 0;
    for (int i1 = 1; i1 < N; i1++)
    {
        l2 += n[i1] * K(i , i1,h) + n[i1 - 1] * K(i , (i1 - 1), h );
    }
    l2 *= h/2;
    return l2;
}


double L_for_vol(const int &N, const double *n, const double &h)
{
    double l2 = 0;
    for (int i1 = 1; i1 < N; i1++)
    {
        l2 += n[i1]*i1*h  + n[i1 - 1] * (i1-1)*h;
    }
    l2 *= h/2;
    return l2;
}

double start_cond(const double &v)
{
    return exp (-v);
}

double analitycal_solution(const double &v, const double & t)
{
    return (1.0 / ((1.0 + t / 2.0) * (1.0 + t / 2.0))) * exp(-v / (1.0 + t / 2.0));
}

int main(int argc, char** argv)
{
	if (argc != 6)
	{
		cout << "5 parameters have been expected" << endl;
		cout << "v_max n_steps time_step n_knots ifpaint"<< endl;
		return -1;
	}
	double v_max = 10.0;
	int N = 101, steps = 100;
	double time_step = 0.01;
	char name_of_file[15] = "values.dat";
	char name_of_picture[30];
	FILE* values;
	int ifpaint = 0;
	struct timeval start, end;

	sscanf(argv[1], "%lf", &v_max);
	sscanf(argv[2], "%d", &steps);
	sscanf(argv[3], "%lf", &time_step);
	sscanf(argv[4], "%d", &N);
	sscanf(argv[5], "%d", &ifpaint);
	double *n_k, *n_k_half, *n_k_one, h, *n_analit, *S;
	double *L1_res_vec, *L2_res_vec, *L1_correction, *L2_correction;

	n_k = (double *) malloc(N*sizeof(double));
	n_k_half = (double *) malloc(N*sizeof(double));
	n_k_one = (double *) malloc(N*sizeof(double));
	n_analit = (double *) malloc(N*sizeof(double));
	S = (double *) malloc(N*sizeof(double));

	h = v_max / (N - 1);
	if (ifpaint)
		system("cd IMG; rm *.png; cd ..");

	for (int i = 0; i < N; i++)
	{
		n_k[i] = start_cond(h * i);
		n_analit[i] = analitycal_solution(h * i, 0.0);
	}
	cblas_daxpy(N, -1.0, n_k, 1, n_analit, 1);
	cout << "Relevant error in starting condition = " << cblas_dnrm2(N, n_analit, 1) / cblas_dnrm2(N, n_k, 1) << ", norm = " << cblas_dnrm2(N, n_k, 1) << endl;

	gettimeofday(&start, NULL);	

	for (int k = 0; k < steps; k++)
	{
	
		for (int i = 0; i < N; i++)
		{
			n_analit[i] = analitycal_solution(h * i, (k + 0.5) * time_step);
			n_k_half[i] = ( L1(N, i, n_k, h) * 0.5 - n_k[i] * L2(N, i, n_k, h) ) * 0.5 * time_step + n_k[i];
		}

		double norm = cblas_dnrm2(N, n_analit, 1);
		cout << "Step " << k + 1 << ", relevant error after predictor = " << cblas_dnrm2(N, n_analit, 1) / norm << ", norm = " << norm << endl;
		if (ifpaint)
			values=fopen (name_of_file, "w+");

		for (int i = 0; i < N; i++)
		{
			S[i] = (L1(N, i, n_k_half, h) * 0.5 - n_k_half[i] *  L2(N, i, n_k_half, h)) ;
			n_k_one[i] = S[i] * time_step + n_k[i];
			if (ifpaint)
				fprintf(values, "%lf \n", n_k_one[i]);
			n_analit[i] = analitycal_solution(h * i, (k + 1.0) * time_step);
		}

		cout << "mass changing derivative = "<< L_for_vol(N, S, h) << endl;
		if (ifpaint)
		{
			system("gnuplot plotting_script1d");
			sprintf(name_of_picture, "mv 1.png IMG/file%03d.png",k+1);
			system(name_of_picture);
			cout<<"saving image data about the step..."<<endl;
			fclose(values);
		}

		double volume=L_for_vol(N, n_k_one, h);
		double analit_volume = L_for_vol(N, n_analit, h);
		cout << "Coagulation volume = " << volume << endl;
		cout << "Analitycal volume = " << analit_volume << endl;



		norm = cblas_dnrm2(N, n_analit, 1);
		cblas_daxpy(N, -1.0, n_k_one, 1, n_analit, 1);
		cout << "Step " << k + 1 << ", relevant error after corrector = " << cblas_dnrm2(N, n_analit, 1) / norm << ", norm = " << norm << endl;



		double * tmp = n_k_one;
		n_k_one = n_k;
		n_k = tmp;
		tmp = NULL;
	}
	gettimeofday(&end, NULL);

	double r_time = end.tv_sec - start.tv_sec + ((double)(end.tv_usec - start.tv_usec)) / 1000000;

	cout << "Average time for step = " << r_time / steps << " seconds" << endl;
	cout << "Full time = " << r_time << "seconds" << endl;
	free(n_k);
	free(n_k_half);
	free(n_k_one);
	free(n_analit);
}
