      subroutine coagulation_step(N_species,
     &N_particle,N_particles_ar_dim,
     &tau,q0,
     &rK_0,i_coag_type,dk_coag_l,
     &y_ar)

      implicit none
c-----input
      integer ::
     &N_species,          !number of species
     &N_particle,         !number of particles
     &N_particles_ar_dim, !dimension of arrays: y_ar 
     &i_coag_type         ! =1 constant coagulation kernel
                          ! =2 brownian coagulation kernel 
      real*8 :: 
     &tau, !time step
     &q0,   !Q0=integral{v0,vmax}q0(v)dv
     &rK_0 !coagulation kernel amplitude

      real*8, dimension(1:N_species,1:N_particle) :: y_ar !`input,output
                                            !aerosols volumes

c      real*8, dimension(1:N_particles_ar_dim,1:N_particles_ar_dim) ::
c     & dk_coag_ar !coagulation Kernel
      real*8 ::
     & dk_coag_l !coagulation Kernel

c-----locals
      integer :: i,j
      real :: rand

      real*8 :: r,y_sum

      integer, dimension(1:N_particles_ar_dim) :: J_ar

      real, dimension(1:N_particles_ar_dim) :: rand_ar

      real*8, dimension(1:N_species,1:N_particles_ar_dim) :: y_ar_old 

      real*8, dimension(1:N_species) :: y_j_vector, y_j_vector_old,
     &y_i_vector, y_i_vector_old

      integer :: k
      real*8 :: dif
      
c--------------------------------------------------------
c     create array of random integers J_ar(1,N_particle)
c     randomly  generated with respective uniform probabilty
c     law over [1,...,N_particle]
c---------------------------------------------------------

c      write(*,*)'in coagulation_step 1'

      do i=1,N_particle
         call random_number(rand)
         J_ar(i)=rand*N_particle+1
      enddo

c      write(*,*)'in coagulation_step 2 J_ar',J_ar

      do i=1,N_particle
         call random_number(rand)
         rand_ar(i)=rand
      enddo

c      write(*,*)'in coagulation_step 3 rand_ar', rand_ar

      y_ar_old=y_ar



c      write(*,*)'in coagulation_step 4'

      do i=1,N_particle
         
         j=J_ar(i)

c         write(*,*)'i,j',i,j

         do k=1,N_species
            y_j_vector_old(k)=y_ar(k,j)
            y_i_vector_old(k)=y_ar(k,i)
         enddo

c         write(*,*)'y_j_vector_old',y_j_vector_old
c         write(*,*)'y_i_vector_old',y_i_vector_old
cSAP170912
          call coagulation_kernel(N_species,
     &          y_i_vector_old,y_j_vector_old,
     &          rK_0,i_coag_type,
     &          dk_coag_l)
c     &          dk_coag_ar(i,j))

c          write(*,*)'k_coag_l',dk_coag_l

c          write(*,*)'i,j,dk_coag_ar(i,j)',i,j,dk_coag_ar(i,j)

          y_sum=0.d0
          do k=1,N_species
             y_sum = y_sum+ y_j_vector_old(k)
          enddo

c          write(*,*)'y_sum',y_sum

c         r=(Q0*dk_coag_ar(i,j)/y_ar_old(j))*tau
          r=(Q0*dk_coag_l/y_sum)*tau
         
c          write(*,*)'j',j,'i',i,'r',r,'rand_ar(i)',rand_ar(i)

c          write(*,*)'Q0',Q0,'dk_coag_ar(i,j)',dk_coag_ar(i,j)
c          write(*,*)'tau',tau,'y_sum',y_sum

c         write(*,*)'i,j,dk_coag_ar(i,j),rand_ar(i),r',
c     &              i,j,dk_coag_ar(i,j),rand_ar(i),r

         if(rand_ar(i).le.r) then
            do k=1,N_species
               y_ar(k,i)=y_ar_old(k,i)+y_ar_old(k,j)
            enddo
         endif      
      enddo

c      dif=0.d0
c      do i=1,N_particle
c        do j=1,N_species
c            dif=dif+dabs(y_ar(j,i)-y_ar_old(j,i))
c        enddo
c      enddo
c      dif=dif/N_particle

c      write(*,*)'in coagulation_step 5 dif=',dif

      return
      end

  
