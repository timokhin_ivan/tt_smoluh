      subroutine coagulation_time_step(N_species,
     &N_particle,N_particles_ar_dim,
     &y_ar,
     &coag_Kernel_0,i_coag_type,dk_coag_l,Q0,
     &tau_coag)
      implicit none
c-----input
      
      integer ::
     &N_species,    !number of species
     &N_particle,  !number of particles 
     &N_particles_ar_dim, !dimension of arrays: y_ar,omega_ar,dk_coag_ar
     &i_coag_type         ! =1 constant coagulation kernel
                          ! =2 brownian coagulation kernel 
      real*8 :: 
     &coag_Kernel_0, !coagulation kernel const 
     &Q0

      real*8, dimension(1:N_species,1:N_particles_ar_dim) :: 
     &y_ar

c      real*8, dimension(1:N_particles_ar_dim,1:N_particles_ar_dim ) ::
c     & dk_coag_ar !coagulation kernel
      real*8  ::
     & dk_coag_l 
c-----output
      real*8 tau_coag
c-----local
      real*8 ::
     & dlabmda_k  !Q0*max(omega_j*K(y_i,y_j)/y)j)
    
c       write(*,*)'in coagulation_time_step i_coag_type',i_coag_type
c       write(*,*)'dk_coag_ar',dk_coag_ar
c--------------------------------------------------------------
c     calculate dlamda_K=max{}
c-------------------------------------------------------------
      call calculate_lamda_k(N_species,
     &N_particle,N_particles_ar_dim,
     &dk_coag_l,y_ar,
     &coag_Kernel_0,i_coag_type,Q0,
     &dlabmda_k)
      
      tau_coag=1.d0/dlabmda_k

c      write(*,*)'coagulation_time_step dlabmda_k,tau_coag',
c     &                                 dlabmda_k,tau_coag

      return
      end

