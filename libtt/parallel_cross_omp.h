#ifndef PARALLEL_CROSS_H
#define PARALLEL_CROSS_H
#include "cross.h"
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "blas.h"
#include <omp.h>
#include "maxvol.h"
#include "stdio.h"
#include "mkl_dfti.h"
struct TCross_Parallel_v1_Parameters
{
    double tolerance;
    int maximal_iterations_number, number_of_checked_elements, max_rank, rank_increase, stop_rank;
    bool memory_strategy, start_from_column;
    TCross_Parallel_v1_Parameters();
};

struct TCross_Parallel_v1_Work_Data{
    bool *I, *J;
    double *current_row, *current_column;
    int omp_column_threads_num, omp_row_threads_num, *omp_column_start, *omp_column_num, *omp_row_start, *omp_row_num;
    TVolume max_volume;
    double global_max;
    TMatrix * matrix;
    TDifferenceMatrix work_matrix;
    TCross_Parallel_v1_Parameters parameters;
    TCross_Parallel_v1_Work_Data(TMatrix *, TMatrix *);
    ~TCross_Parallel_v1_Work_Data();
};

class TCross_Parallel_v1: public TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>
{
    private:
        double *U, *V, *C, *hat_A_inv, *RT, tolerance, norm, *AR, *CAT;
        int * rows_numbers, * columns_numbers;
        void Prepare_Data(TCross_Parallel_v1_Work_Data &, const TCross_Parallel_v1_Parameters &);
        void Search_Max_Volume(TCross_Parallel_v1_Work_Data &);
        bool Stopping_Criteria(TCross_Parallel_v1_Work_Data &);
        void Update_Cross(TCross_Parallel_v1_Work_Data &);
        void get_diff_column(const int &, const TCross_Parallel_v1_Work_Data &, double *&);
        void get_diff_row(const int &, const TCross_Parallel_v1_Work_Data &, double *&);

	double *smol_conv(double* &);
    public:
	double *matvec(double *&x, const char &option='f');
        double *smol_conv_trapezoids(double *&);
	double *smol_conv_discrete(double *&);
	double value(const int &, const int &);
        int get_row_number(const int &) const;
        int get_column_number(const int &) const;
        TCross_Parallel_v1();
        ~TCross_Parallel_v1();
        const double *export_C();
        const double *export_hat_A_inv();
        const double *export_RT();
        const double *export_CAT();
        const double *export_AR();
};
#endif
