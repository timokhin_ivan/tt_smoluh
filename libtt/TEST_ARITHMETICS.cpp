#include "tensor.h"
#include "tensor_train.h"
#include <stdio.h>

// Declare target functions for TT-cross 

// Function 1
class TTest1 : public TTensor {
	public:
                // f(x_1, x_2, ... ,x_d) = 10.0 +(x_1 + x_2 + ... + x_d
                // x_i at uniform mesh in [0,1]^d
		double operator[](int *point) {
			double s = 0;
			for (int i = 0; i < this->get_dimensionality(); i++)
				s += point[i] * (1.0 / (this->get_mode_size(i)));
			return 10.0 + s;
		}
		TTest1():TTensor() {
		}
		TTest1(const int &d , int * &m):TTensor(d,m) {
		}
};

// Function 2
class TTest2 : public TTensor{
	public:
                // f(x_1, x_2, ... ,x_d) = 10.0 + e^{-(x_1 + x_2 + ... + x_d)}
                // x_i at uniform mesh in [0,1]^d
		double operator[](int *point) {
			double s = 0;
			for (int i = 0; i < this->get_dimensionality(); i++)
				s += point[i] * (1.0 / (this->get_mode_size(i)));
			return 10.0 + exp(-s);
		}
		TTest2():TTensor() {
		}
		TTest2(const int &d , int * &m):TTensor(d,m) {
		}
};
// EXAMPLE
int main(int argc, char **argv) {
	int i, j, N, d, *modes_sizes1, *modes_sizes2;
        if (argc < 3)
        {
            printf("args: N d\n");
            return 1;
        }
	sscanf(argv[1], "%d", &N);                       // N is mode size along axis
	sscanf(argv[2], "%d", &d);                       // dimensionality of the function
	modes_sizes1 = (int *) malloc( d * sizeof(int)); // allocate mode sizes array
	for (int i = 0; i < d; i++ )                     //
		modes_sizes1[i] = N;                     // fill the modes
	TTest1 test1(d, modes_sizes1);                   // declare first tensor for approximation
                                                         //
	modes_sizes2 = (int *) malloc( d * sizeof(int)); //
	for (int i = 0; i < d; i++ )                     //
		modes_sizes2[i] = N;                     //
	TTest2 test2(d, modes_sizes2);                   // declare second tensor for approximation
                                                         //
	TTensorTrainParameters parameters;               // parameters structure for TT Cross approximation 
	parameters.tolerance = 1e-6;                     // convergence parameter
	parameters.maximal_iterations_number = 0;        // number of iterations. iterates until convergence if set as 0
                                                         //
	TTensorTrain tt1, tt2,tt3;                       // declare objects for tensor trains
	tt1.Approximate(&test1, parameters);             // call TT Cross approximation of target function 1
	tt2.Approximate(&test2, parameters);             // call TT Cross approximation of target function 2
	printf("Approximated tt1 and tt2\n");            //                                                 
	printf("tt1\n");                                 //
	for (int i = 0; i < d; i++)                      //
            printf("rank tt1 = %d\n", tt1.get_rank(i));  // print its tt-ranks 
	printf("\n\n\ntt2\n\n");                         //
	for (int i = 0; i < d; i++)                      //
            printf("rank tt2 = %d\n", tt2.get_rank(i));  //
	double alpha = 2.0;                              //
	tt3 = (tt1 * alpha) * tt2 + alpha * tt2 - tt1;   // TEST TENSOR TRAIN ARITHMETICS.       
	//tt3 = (scalar * tt1) * tt2 + tt2;              // Right-hand multiplication of TT by scalar needs to be checked
	printf("Finished with operations\n");            //
	printf("\n\n\ntt3\n\n");                         //
	for (int i = 0; i < d; i++)                      //
            printf("rank tt3 = %d\n", tt3.get_rank(i));  //
        printf("SVDCompress\n");                         // TEST SVD Compression        
        tt3.SVDCompress(parameters.tolerance);           // Tolerance parameter for compression
	printf("Finished SVDCompress\n");                //
	printf("\n\n\ntt3\n\n");                         // 
	for (int i = 0; i < d; i++)                      // print updated tt-ranks
            printf("rank tt3 = %d\n", tt3.get_rank(i));  // 
	return 0;                                        //
}
