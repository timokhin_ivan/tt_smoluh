#include <iostream>
#include <sstream>
#include "tensor.h"
#include "tensor_train.h"
#include <math.h>
#include <omp.h>
#include <stdlib.h>


class Series1 : public TTensor {
    public:
        double t;
        double operator[](int *point) 
        {
            unsigned long long int n = 0;
            for(int i = 0; i < this->get_dimensionality(); i++) 
            {
                n <<= 1;
                n += point[i];
            }
            //std::cout << n << ' ' << exp(-t * n * n) * ((n % 2)? -1 : 1) << std::endl;
            //return exp(-t * n * n) * ((n % 2)? -1 : 1);
            //return 4.0 / (2 * n + 1) * ((n % 2)? -1 : 1);
            return exp(-t * (double ) n * (double ) n) * (double)((n % 2)? -1 : 1);
        }
        Series1(): TTensor(), t(1.0) {}
        Series1(const int& d, int* &m, const double& T): TTensor(d, m), t(T) {}

};


//1)sum from 0 to inf ((-1)^n * a * exp(-ctn^2))
//assume a = c = 1
//2) (-1)^n  * n^2 * exp(-tn^2)

int main(int argc, char** argv) {
    if(argc < 3) {
        std::cout << "args: e, t, p\n";
        return 0;
    }
    double e, t;
    const int p = 1;
    std::stringstream ss1(argv[1]);
    ss1 >> e;
    std::stringstream ss2(argv[2]);
    ss2 >> t;
//    std::stringstream ss3(argv[3]);
 //   ss3 >> p;
    unsigned long long int n = 0;
    if(p == 1) n = sqrt(log(1/e) / t);
//    if(p == 2) n = max(1/sqrt(t), (1 + sqrt(1 + 4 * t * log(1/e)))/(2 * t) );
    //n -> 2^d
    //n = 1048576;
    int c = 0;
    n--;
    while(n) 
    {
        c++;
        n >>= 1;
    }
    n = 1;
    for(int i = 0; i < c; i++) {
        n <<= 1;
    }
    //n = 2^c;
    std::cout << n << ' ' << c << std::endl;
    int *m = new int [c];
    for(int i = 0; i < c; i++) m[i] = 2;
    TTensorTrainParameters param;
    param.tolerance = e;
    param.maximal_iterations_number = 0;
    TTensorTrain tt;
    double cl = omp_get_wtime();
    if(p == 1) {
        Series1 s(c, m, t);
        tt.Approximate(&s, param);
    }
    
    /*
    if(p == 2) {
        Series2 s(c, m, t);
        tt.Approximate(&s, param);
    }*/
    TTensorTrain mask(tt, 1);
    std::cout << "TT SUM = " << tt.dot(mask) << " TIME = " << (omp_get_wtime() - cl) << std::endl;

    for (int i = 0; i < c; i++)
        std::cout << "TT rank " << i << " = " << tt.get_rank(i) << std::endl;
    /*int point0[2] = {0, 0};
    std::cout << tt[point0] << std::endl;
    int point1[2] = {1, 0};
    std::cout << tt[point1] << std::endl;
    int point2[2] = {0, 1};
    std::cout << tt[point2] << std::endl;
    int point3[2] = {1, 1};
    std::cout << tt[point3] << std::endl;
    */
    cl = omp_get_wtime();
    double sum = 0;
    if(p == 1) {
        for(unsigned long long int i = 0; i < n; i++) {
            sum += exp(-t * (double)i * (double)i) * ((i % 2)? -1 : 1);            
            //sum += 4.0 / (2.0 * (double) i + 1.0) * ((i % 2)? -1 : 1);
        }
    }
/*    if(p == 2) {
        for(int i = 0; i < n; i++) {
            sum += (double)i * (double)i * exp(-t * (double)i * (double)i) * ((i % 2)? -1 : 1);
        }
    }*/
    std::cout << "DIRECT SUM = " << sum << " TIME = " << (omp_get_wtime() - cl) << std::endl;
    return 0;
}
