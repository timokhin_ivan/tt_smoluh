#include <iostream>
#include <sstream>
#include "tensor.h"
#include "tensor_train.h"
#include <math.h>
#include <omp.h>
#include <stdlib.h>
double alpha = 1.0;
double beta = 1e8;
class Series1 : public TTensor {
    public:
        double t;
        double operator[](int *point) {
            unsigned long long int n = 0;
            for(int i = 0; i < this->get_dimensionality(); i++) {
                n <<= 1;
                n += point[i];
            }
            //n*=2;
            double n1 = (double) n + 1;
            double c = exp(-alpha * t * n1 * n1) * beta * n1  * n1 * (double)((n % 2)? -1 : 1);
            //n++;
            //c += exp(-t * (double)n * (double)n) * (double)n  * (double)n * ((n % 2)? -1 : 1);
            return c;
        }
        Series1(): TTensor(), t(1.0) {}
        Series1(const int& d, int* &m, const double& T): TTensor(d, m), t(T) {}

};

void to_power_of_2 (unsigned long long int &n, int &c) {
    c = 0;
    n--;
    while(n) {
        c++;
        n >>= 1;
    }
    n = 1;
    for(int i = 0; i < c; i++) {
        n <<= 1;
    }
}  //n = 2^c;

//U can try to sum two elements of a series in one tensor element
//It will reduce dimensionality by 1
//To do it 'remove' all comments (remove this - '//')
//3 comments in class declaration and 1 in main()
int main(int argc, char** argv) {
    if(argc < 3) {
        std::cout << "args: e, t\n";
        return 0;
    }
    double e, t;
    std::stringstream ss1(argv[1]);
    ss1 >> e;
    std::stringstream ss2(argv[2]);
    ss2 >> t;
    unsigned long long int n = 0;
    n = 1/sqrt(t);
    int c = 0;
    to_power_of_2(n, c);
    for(; exp(-alpha * t * (double)n * (double)n) * (double)n * (double)n * beta > e; n *= 2, c++) 
    {}
    
    std::cout << n << ' ' << c << std::endl;
    /////////////////////////////
    //c--; 
    int *m = new int [c];
    for(int i = 0; i < c; i++) 
        m[i] = 2;
    TTensorTrainParameters param;
    param.tolerance = e;
    param.maximal_iterations_number = 0;
    TTensorTrain tt1;
    double cl = omp_get_wtime();
    Series1 s(c, m, t);
    tt1.Approximate(&s, param);
    m = new int [c];
    for(int i = 0; i < c; i++) 
        m[i] = 2;
    TTensorTrain mask(tt1, 1);
    for (int i = 0; i < c; i++)
        std::cout << "TT1 rank " << i << " = " << tt1.get_rank(i) << std::endl;
    std::cout << "Series sum " << tt1.dot(mask) << " time " << (omp_get_wtime() - cl) << " tt result" << std::endl;
    cl = omp_get_wtime();
    double sum = 0.0;
    for(unsigned long long int i = 0; i < n; i++)
    {
        double i1 = (double) i + 1;
        double c = exp(-alpha * t * i1 * i1) * i1 * i1 * beta * (double) ((i % 2)? -1 : 1);
        sum += c;
    }
    std::cout << "Series sum = " << sum << " time "  << (omp_get_wtime() - cl) << " direct sum result" << std::endl;
    return 0;
}
